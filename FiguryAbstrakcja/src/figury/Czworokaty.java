package figury;

public abstract class Czworokaty {
protected double boki[];
	
	protected Czworokaty(double... boki) {
		this.boki = boki;
	}
	
	public double[] getBoki() {
		return boki;
	}
	
	public void setBoki(double... boki) {
		this.boki = boki;
	}
	
	public abstract double obwod();
	
	public double pole() {
		try {
			return boki[0]*boki[0];
		}
		catch (Exception e) {
			
		}
		return 0;
	}

}
