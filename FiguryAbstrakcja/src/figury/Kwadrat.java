package figury;

public class Kwadrat extends Czworokaty {

	public Kwadrat() {
		super(0);
	}
	
	public Kwadrat(double a) {
		super(a);
	}
	
	public void abstrakcja() {
		System.out.println("JESTEM Z KLASY PROSTOKAT!");
	}
	
	public void dodaj(int a) {
		System.out.println("Suma podanych liczb: " + (a));
	}
	
	
	
	@Override
	public double obwod() {
		// TODO Auto-generated method stub
		try {
			return 4*getBoki()[0];
		}
		catch (Exception e) {
			System.err.println("Nie zdefiniowano tablicy bądz tablica posiada mniej niż dwa elementy!");
		}
		return 0;
	}
	

}
