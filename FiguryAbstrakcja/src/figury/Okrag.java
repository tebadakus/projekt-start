package figury;


/*
 * Tutaj mamy przykład klasy dziedziczącej po innej klasie
 * (tzw. rozszerzenie o własności klasy bazowej). W tym wypadku
 * klasa dziedzicząca (Okrag) pozyskuje jako swoje 
 * wszystkie składowe publiczne i chronione (protected).
 */
public class Okrag extends Owale {
	private double r;
	
	public Okrag() {
		System.out.println("To jest konstruktor Okregu!");
	}
	
	public Okrag(double r) {
		this.r = r;
	}
	
	public void setR(double r) {
		this.r=r;
	}
	
	public double getR() {
		return r;
	}
	
	public double obwod() {
		//ponieważ pi jest pole protected w klasie Owale
		//bez problemu może tutaj zostać pobrane - nie ma
		//potrzeby pobierania wartości poprzez metodę
		return 2*pi*r;
	}
	
	public double pole() {
		return pi*r*r;
	}
}
