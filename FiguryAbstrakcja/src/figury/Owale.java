package figury;


public class Owale {
	/*
	 * Slowo final powoduje, że "zmienna" staje się
	 * wartością stałą, której nie możemy zmodyfikować
	 * przez cały okres działania programu
	 * 
	 * Słowo static powoduje, że zmienna/stała 
	 * istnieje przez CAŁY OKRES DZIAŁANIA PROGRAMU.
	 * Oznacza to, że nie musimy tworzyć obiektu z klasy
	 * (poprzez operato new) tylko możemy się do takiej
	 * waartości odwołać bezpośrednio do klasy
	 */
	protected static final double pi=3.14;

	/*
	 * Żeby móc skorzystać z wartości statycznej prywatnej
	 * metoda pobierająca taką wartość (bądz wykorzystujaca ja)
	 * także musi być statyczna. 
	 */
	public static double getPi() {
		return pi;
	}
	
	public Owale() {
		System.out.println("To jest konstruktor Owale!");
	}
	
	
	
}
