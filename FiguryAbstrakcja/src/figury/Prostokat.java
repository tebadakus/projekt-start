package figury;

	public class Prostokat extends Czworokaty {

		public Prostokat() {
			super(0,0);
		}
		
		public Prostokat(double a, double b) {
			super(a,b);
		}
		
		public void abstrakcja() {
			System.out.println("JESTEM Z KLASY PROSTOKAT!");
		}
		
		public void dodaj(int a,int b) {
			System.out.println("Suma podanych liczb: " + (a+b));
		}
		
		
		public double pole() {
			try {
				return boki[0]*boki[1];
			}
			catch (Exception e) {
				
			}
			return 0;
		}
		
		@Override
		public double obwod() {
			abstrakcja();
			try {
				return 2*getBoki()[0]+2*getBoki()[1];
			}
			catch (Exception e) {
				System.err.println("Nie zdefiniowano tablicy bądz tablica posiada mniej niż dwa elementy!");
			}
			return 0;
		}

	}


