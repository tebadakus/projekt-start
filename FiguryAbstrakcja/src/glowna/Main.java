package glowna;

import figury.Czworokaty;

 import figury.*;

public class Main {
	public static void main(String[] args) {
	    System.out.println(Owale.getPi());
		//pole protected nie bedzie widziane w tym kontekscie
	    //System.out.println(Owale.pi);
		
	    Elipsa elipsa = new Elipsa();
	    elipsa.setA(10);
	    elipsa.setB(2);
	    System.out.println(elipsa.pole());
	    System.out.println(elipsa.obwod());
	    
	    Elipsa elipsa2 = new Elipsa(12.4,4);
	    System.out.println(elipsa2.pole());
	    System.out.println(elipsa2.obwod());
	    
	    Czworokaty prostokat = new Czworokaty() {
	    	public void abstrakcja() {
	    		System.out.println("Jestem abstrakcją!");
	    	}
	    	
	    	public double obwod() {
	    		abstrakcja();
	    		try {
	    			return 2*getBoki()[0]+2*getBoki()[1];
	    		}
	    		catch (Exception e) {
	    			System.err.println("Nie zdefiniowano tablicy bądz tablica posiada mniej niż dwa elementy!");
	    		}
	    		return 0;
	    	}
		};
		prostokat.setBoki(2,3);
		System.out.println("Obwod prostokata: " + prostokat.obwod());
		
		Prostokat p = new Prostokat(4,5);
		p.obwod();
		p.dodaj(14, 1);
		
		Czworokaty p2 = new Prostokat(4,4);
		p2.pole();
		System.out.println("Obwod prostokata: " + p.obwod());
		System.out.println("Pole prostokata: "+p.pole());
		
		Kwadrat k=new Kwadrat(3);
		//k.obwod();
		System.out.println("Obwod kwadratu: "+k.obwod());
		System.out.println("Pole kwadratu: "+k.pole());
				
		Trapez t=new Trapez(5,3,4,4);
		System.out.println("Obwod trapeza: "+t.obwod());
	}
}
