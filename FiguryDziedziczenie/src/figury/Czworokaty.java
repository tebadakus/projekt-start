package figury;

public class Czworokaty {
	public Czworokaty() {
		System.out.println("To jest konstruktor clsCzworokaty!");
	}
	
    static double bokA,
    			  bokB,
    			  bokC,
    			  bokD;
	
	public String obwodCzworokatu() {
		double  bA=this.bokA,
				bB=this.bokB,
				bC=this.bokC,
				bD=this.bokD;
		
		return "Obwod Czworokatu o bokach: "+bA+", "+bB+", "+bC+", "+bD+"= "+(bA+bB+bC+bD);
	}
	
	
	public double getBokA() {
		return bokA;
	}

	public void setBokA(double bokA) {
		this.bokA = bokA;
	}

	public double getBokB() {
		return bokB;
	}

	public void setBokB(double bokB) {
		this.bokB = bokB;
	}

	public double getBokC() {
		return bokC;
	}

	public void setBokC(double bokC) {
		this.bokC = bokC;
	}

	public double getBokD() {
		return bokD;
	}

	public void setBokD(double bokD) {
		this.bokD = bokD;
	}

	
}
