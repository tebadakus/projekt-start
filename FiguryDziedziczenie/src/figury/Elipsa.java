package figury;

	public class Elipsa extends Okrag {
		private double a,b;
		
		public Elipsa() {
			System.out.println("To jest konstruktor Elipsy!");
		}
		
		public Elipsa(double... p) {
			super();
			a=p[0];
			b=p[1];
			
		}
		
		public void setA(double a) {
			this.a=a;
		}
		
		public void setB(double b) {
			this.b=b;
		}
		
		public double pole() {
			return pi*a*b;
		}
		
		public double obwod() {
			return (3/2*(a+b)- Math.pow(a*b,1/2))*pi;
		}
	

}
