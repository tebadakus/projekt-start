package figury;

/*
 * przykład definicji klasy abstrakcyjnej. Pozwala ona na tworzenie szablonu (ramy)
 * klasy, która z kolei pozwala na przedefiniowanie niektórych elementów w klasach pochodznych.
 * Generalnie w klasach tego typu nie zawsze musimy tworzyć definicję wszystkich metod - możemy
 * niektóre z nich uczynić jedynie deklaracjami (poprzedzając je słowem abstract). 
 * 
 * Niektóre z metod możemy zdefiniować, jednak to nie zamyka nam opcji do redefinicji (działa to tak
 * jak w standardowych klasach). 
 */

public abstract class Czworokaty {
	private double boki[];
	
	protected Czworokaty(double... boki) {
		this.boki = boki;
	}

	public double[] getBoki() {
		return boki;
	}
	
	public void setBoki(double... boki) {
		this.boki = boki;
	}
	
	public abstract double obwod();
	
	public double pole() {
		try {
			return boki[0]*boki[0];
		}
		catch (Exception e) {
			
		}
		return 0;
	}
}
