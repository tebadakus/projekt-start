package figury;

/*
 * Klasy mogą dziedziczyć po klasach, które z kolei
 * dzieczą po jeszcze innych klasach. Dzięki temu
 * tak powstała nowa klasa może zawierać właściwości
 * więcej niż jednej klasy bazowej (domyślnie Java
 * pozwala na dziedziczenie jedynie po jednej klasie)
 * INFO: Klasa elipsa mogła dziedziczyć po klasie
 * Owale.
 */

public class Elipsa extends Okrag {
	private double a,b;
	
	public Elipsa() {
		System.out.println("To jest konstruktor Elipsy!");
	}
	
	/*
	 * Poniższy argument to inny rodzaj deklaracji zmiennej typu tablicowego
	 * Posiada on jednak jedną wyższość nad ekwiwalentem, tj.
	 * 
	 * double[] p
	 * 
	 * Użytkownik (programista) może bez przeszkód podawać kolejne wartości jako
	 * kolejne parametry metody 
	 * 
	 * Elipsa(13,2.4,1,6.7)
	 * 
	 * Podczas gdy w innym wypadku musielibyśmy podwać tablicę elementów (podstawiać zmienną tablicową)
	 * 
	 * Elipsa(new double[] {4,5,6,6,7}) 
	 * 
	 * 
	 */
	public Elipsa(double... p) {
		/*
		 * Poniżej przykład wywołania konstruktora 
		 * klasy bazowej. W ten sposób możemy bazować na
		 * obiekcie z odpowiednimi parametrami startowymi
		 * (poniżej przykład ustawienia wartości r klasy
		 * Okrag na wartość 2)
		 */
		super(2);
		a=p[0];
		b=p[1];
		
	}
	
	public void setA(double a) {
		this.a=a;
	}
	
	public void setB(double b) {
		this.b=b;
	}
	
	
	
	/*
	 * Metoda nadpisuje działania metody klasy bazowej
	 * Okrag. Jeżeli w tym miejscu nie nadpisalibyśmy 
	 * tej metody to otrzymywalibyśmy pole liczne dla
	 * okręgu (pola okręgu dla elipsy nie da się
	 * w ten sposób obliczyć). 
	 * W programowaniu powszechne jest nadpisywanie
	 * metod z klas bazowych. 
	 */
	public double pole() {
		System.out.println("Pole klasy Elipsa");
		return pi*a*b;
	}
	
	/*
	 * Metoda nadpisuje zachowanie metody o tej samej 
	 * nazwie z klasy bazowej Okrag (por. komentarz 
	 * powyżej)
	 */
	public double obwod() {
		System.out.println("Obwod klasy Elipsa");
		return (3/2*(a+b)- Math.pow(a*b,1/2))*pi;
	}
}
