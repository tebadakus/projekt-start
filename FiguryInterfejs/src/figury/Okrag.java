package figury;

/*
 * Tutaj mamy przykład klasy dziedziczącej po innej klasie
 * (tzw. rozszerzenie o własności klasy bazowej). W tym wypadku
 * klasa dziedzicząca (Okrag) pozyskuje jako swoje 
 * wszystkie składowe publiczne i chronione (protected).
 */
public class Okrag extends Owale {
	private double r;
	
	/*
	 * Poniższa metoda to tzw. konstruktor klasy. Dzięki konstruktorom
	 * programista może wymusić pewne określone stany i/lub wartości 
	 * poszczególnych składowych obecnie konstruowanej klasy. Nawet jeżeli
	 * nie zostanie zdefiniowany - KAŻDA KLASA POSIADA SWOJ KONSTRUKTOR.
	 * Jeżeli nie jest zdefiniowany - tworzony jest tzw. domyślny (niejawnie - bez wiedzy programisty)
	 * Jakiekolwike utworzenie któregokolwiek konstruktora powoduje skasowanie kostruktora
	 * domyślnego. 
	 * 
	 * Konstruktory mogą przyjmować parametry (przykład poniżej). Mogą posiadac zmienione 
	 * modyfikatory zasięgu (nie muszą być public!). Od standardowych metod odróżnia je to, 
	 * że mają nazwę identyczną z nazwą klasy oraz nie posiadają zwracanego typu!
	 */
	
	public Okrag() {
		System.out.println("To jest konstruktor Okregu!");
	}
	
	public Okrag(double r) {
		this.r = r;
	}
	
	public void setR(double r) {
		this.r=r;
	}
	
	public double getR() {
		return r;
	}
	
	public double obwod() {
		//ponieważ pi jest pole protected w klasie Owale
		//bez problemu może tutaj zostać pobrane - nie ma
		//potrzeby pobierania wartości poprzez metodę
		System.out.println("Obwod klasy Okrag");
		return 2*pi*r;
	}
	
	public double pole() {
		System.out.println("Pole klasy Okrag");
		return pi*r*r;
	}
}
