package figury;

public class Prostokat extends Czworokaty {

	public Prostokat() {
		super(0,0);
		
	}
	
	public Prostokat(double a, double b) {	
		super(a,b);
	}
	
	public void abstrakcja() {
		System.out.println("JESTEM Z KLASY PROSTOKAT!");
	}
	
	public void dodaj(int a,int b) {
		System.out.println("Suma podanych liczb: " + (a+b));
	}
	
	/*
	 * Ponieważ kalsa dziedziczy po abstrakcyjnej klasie Czworokaty, w której zadeklarowano
	 * abstrakcyjną metodę obwod() MUSIMY w naszej pochodnej klasie zdefiniować działanie
	 * tejże metody. Przeważnie definicję takiej metody poprzedza się dyrektywą
	 * 
	 * @Override
	 * 
	 * czyli nadpisanie (nie jest to obligatoryjne). 
	 * 
	 * Tgo typu rozwiązanie daje pewność, że wszystkie dziedziczące klasy MUSZĄ posiadać
	 * własną implementację wskazanej metody (metod), jednak każda z nich moze to wykonywać w inny
	 * sposób.
	 */
	
	@Override
	public double obwod() {
		abstrakcja();
		try {
			return 2*getBoki()[0]+2*getBoki()[1];
		}
		catch (Exception e) {
			System.err.println("Nie zdefiniowano tablicy bądz tablica posiada mniej niż dwa elementy!");
		}
		return 0;
	}

}
