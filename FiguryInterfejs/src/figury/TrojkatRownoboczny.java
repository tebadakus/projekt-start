package figury;

public class TrojkatRownoboczny extends TrojkatyCA {

	public TrojkatRownoboczny() {
		super(0);
	}
	
	public TrojkatRownoboczny(double a) {
		super(a);
	}
	
	@Override
	public double obwod() {
		// TODO Auto-generated method stub
		return 3*getBoki()[0];
	}
	@Override
	public double pole() {
		double p=(obwod())*0.5; //polowa obwodu
		return Math.sqrt(p*(p-getBoki()[0])*(p-getBoki()[0])*(p-getBoki()[0])); // Wzor Herona
		
	}
}
