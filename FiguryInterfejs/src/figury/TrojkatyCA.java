package figury;

public abstract class TrojkatyCA implements Trojkaty {

private double boki[];
	
	protected TrojkatyCA(double...ds) {
		this.boki=ds;
	}
	
	
	public double[] getBoki() {
		return boki;
	}


	public void setBoki(double[] boki) {
		this.boki = boki;
	}


	public double pole() {
		
		double p=(obwod())*0.5; //polowa obwodu
		return Math.sqrt(p*(p-boki[0])*(p-boki[1])*(p-boki[2])); // Wzor Herona
	}

	public double obwod() {
		// TODO Auto-generated method stub
		return boki[0]+boki[1]+boki[2];
	}
	
}
