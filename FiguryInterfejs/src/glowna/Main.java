/*
 * Projekt pokazuje działanie dziedziczenia obiektów oraz implementację własnych konstruktorów.
 * !!!!!!!!!!
 * Zadanie: to stworzenie klasy trojkaty oraz czworokąty, z których dziedziczyłyby figury geometryczne
 * takie jak trójkąt równoboczny, rónoramienny (trojkaty) czy kwadrat, prostokat (czworokaty). 
 */


/*
 * Projekt ma na celu pokazanie działania obiektowości w języku Java.
 * W kodzie użyto zarówno klas podstawowych, rozszerzonych (dziedziczących)
 * jak i abstrakcyjnych. Każda z nich zawiera swój opis (w swoich plikach).
 * 
 * Kod został pdzielony na dwie paczki - figury (zawiera klasy figur - bazowe oraz
 * pochodne) oraz glowne (w tym momencie tylko obecny plik Main.java)
 */
package glowna;

import figury.*;

/*
 * Java to język niemal w pełni obiektowy. Każdy plik Java to minimum jedna klasa,
 * której nazwa MUSI być tożsama z nazwą pliku (np. obecny plik nazywa się Main.java i
 * zawiera w sobie główną klasę o nazwie Main). Każdy plik musi zawierać przynajmniej jedną klasę
 * (a jeden plik w projekcie musi posiadać klasę publiczną).
 * Klasy mogą zawierać dowolną ilość zmiennych (dowolnego typu) jak i dowolną ilość funkcji.
 * Zmienne w klasach noszą nazwę pól/właściwości.
 * Funkcje w klasach noszą nazwę metod.
 * Pola i metody są określane zbiorczym mianem składowe.
 */

public class Main {
	/*
	 * Jeżeli projekt ma się uruchamiać (stać się programem) to jedna z klas MUSI zawierać 
	 * metodę main. Metoda ta wywoływana jest przez maszynę wirtualna Java i to jej kod
	 * wykonywany jest na JVM (Java Virtual Machine). Metoda main musi zawierać minimum 
	 * jeden parametr typy String[]. Parametr ten pzyjmuje parametry podawane przy uruchamianiu programu -
	 * są to parametry wejściowe (startowe). Przykład parametrów:
	 * 
	 * C:\<nazwa_programu> -plik moj.txt -login user
	 * 
	 * gdzie parametrami będą:
	 * -plik
	 * moj.txt
	 * -login
	 * user
	 * Odwołując się do tablicy args możemy odczytać te parametry i wykorzystać w programie
	 * 
	 * Należy zauważyć, że metoda main jest typu static - dzieje się tak, gdyż MUSI ona istnieć przez cały okres działania 
	 * programu. Musi też być niezależna od utworzenia w pamięci obiektu klasy Main (bo jej de facto nie tworzymy!). Więcej 
	 * o słowie static można przeczytać w pliku klasy Owale.java
	 */
	public static void main(String[] args) {
		//pomimo, że nie utworzyliśmy obiektu z klasy Owale to możemy odwołać się
		//do publicznej metody getPi(), która jest zmodyfikowana słowe static 
	    System.out.println(Owale.getPi());
		//pole protected nie bedzie widziane w tym kontekscie - poniższa linia wywołałaby błąd
	    //System.out.println(Owale.pi);
		
	    //utworzenie nowego obiektu o nazwie elipsa z klasy o nazwie Elipsa
	    Elipsa elipsa = new Elipsa();
	    //ustawiamy promień a 
	    elipsa.setA(10);
	    //ustawiamy promień b
	    elipsa.setB(2);
	    //wypisujemy pole naszej elipcy
	    System.out.println(elipsa.pole());
	    //wypisujemy obwod naszej elipsy
	    System.out.println(elipsa.obwod());
	    
	    //przykład utworzenia zmiennej elipsa2, dla której użyliśmy konstruktora z parametrami
	    //dzięki temu nie musimy używać metod setA() oraz setB() -> boki zostaną ustawione właśnie
	    //przez parametry domyślne
	    Elipsa elipsa2 = new Elipsa(12.4,4);
	    System.out.println(elipsa2.pole());
	    System.out.println(elipsa2.obwod());
	    
	    /*
	     * Przykład utworzenia obiektu z abstrakcyjnej klasy Czworokaty. W języku Java jeżeli klasa jest 
	     * typu abstakcyjnego to nie możemy z niej, w prosty sposób, utworzyć obiektu. Większość środowisk
	     * Java od razu podpowie nam, że musimy doimplementować brakujące elemeny klasy (bądz zaimplementwać
	     * nowe rozwiązania) i dopiero po tym będziemy mieli możliwość korzystania z obiektu. 
	     */
	    Czworokaty prostokat = new Czworokaty() {
	    	//przykład utworzenia zupełnie nowej metody w ciele abstrakcyjnej klasy
	    	//tego typu metoda będzie mogła być użytkowana TYLKO W OBECNEJ DEFINICJI
	    	//w związku z powyższym nie gra roli czy użyjemy modyfikatora zasięgu public,
	    	//protected czy private (możemy też nie użyć żadnego)
	    	public void abstrakcja() {
	    		System.out.println("Jestem abstrakcją!");
	    	}
	    	//definicja działania metody obwod, ktora jest metodą abstrakcyjną klasy Czworokat
	    	public double obwod() {
	    		//w tym kontekście możemy wywołać metodę abstrakcja - wewnątrz innej metody klasy
	    		abstrakcja();
	    		//zakładając, ze pole boki klasy Czworokat może być puste tworzymy klauzulę warunkową try...catch
	    		try {
	    			//ponizeważ boki są prywatne, w obecnej definicji nie możemy z nich korzystać (w rozumieniu
	    			//języka Java tworzona przez nas definicja klasy jest klasą pochodną od abstrakcyjnej)
	    			//dlatego też wykorzystujemy tutaj metodę getBoki(), która zwraca wartość tablicy boki[]
	    			//oznacza to, że po wywołaniu metody możemy używać indeksów tablicy tak jakbyśmy operowali na
	    			//samej zmiennej
	    			return 2*getBoki()[0]+2*getBoki()[1];
	    		}
	    		catch (Exception e) {
	    			System.err.println("Nie zdefiniowano tablicy bądz tablica posiada mniej niż dwa elementy!");
	    		}
	    		return 0;
	    	}
		};
		//utworzony już obiekt otrzyma nowe parametry boków - długość 2 i 3
		prostokat.setBoki(2,3);
		System.out.println("Obwod prostokata: " + prostokat.obwod());
		
		//tworzenie obiektu z klasy Prostokat - pochodnej klasy Czworokat. Ponieważ posiada ona już wszystkie niezbędne
		//definicje - nie musimy w tym miejscu definiować ciała obiektu
		Prostokat p = new Prostokat();
		p.obwod();
		//nasz prostokąt posiada dodatkową metodę dodaj, ktorej nie ma w klasie abstrakcyjnej bazowej
		p.dodaj(14, 1);
		
		//Java dopuszcza tworzenie obiektów klasy pochodonej w zmiennych klasy bazowej (w tym wypadku 
		//nawet abstrakcyjnej). Wszystkie składowe wspólne dla obu klas będą działały bez zarzutu
		//(lecz nie będzie np. działać metoda dodaj - bo nie ma jej definicji i/lub deklaracji w
		//klasie Czworokaty
		Czworokaty p2 = new Prostokat(4,4);
		System.out.println("Obwod prostokata: " + p2.obwod());
	
		Kwadrat k1=new Kwadrat(9);
		System.out.println("To jest obwód kwadratu: "+k1.obwod()+". To jest pole kwadratu: "+k1.pole());
	
		TrojkatRownoboczny tr=new TrojkatRownoboczny(7);
		System.out.println("To jest obwód trójkąta równobocznego: "+ tr.obwod());
		
		TrojkatyCA tr2=new TrojkatRownoboczny(7);
		System.out.println("Pole trojkata: "+tr.pole());
		System.out.println("Pole trojkata obj deklaracja trojkatyCA: "+ tr2.pole());
		
		Trojkat t=new Trojkat(8,7,9);
		System.out.println("Pole trojkata: "+ t.pole()+" Obwod trojkata: "+t.obwod());
	}
}
