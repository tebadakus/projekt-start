package losowanieLottoFunkcje;

import java.util.Random;
import java.util.Scanner;


public class LosowanieLottoFunkcje {

	// funkcja losowania pojedynczej liczby z przedziału.
//****************************************************************
	static int losPrzedzial(int minimum, int maksimum) {
		
		int los=new Random().nextInt(maksimum-minimum+1)+minimum;
		return los;
	}
//****************************************************************
	
	// funkcja losowania z przedziałem z eliminacją duplikatów
//*****************************************************************************************
	static void losPrzedzialBezDuplikatow(int ilosc, int maksimum, int minimum) {
		int numery[];

	numery = new int[ilosc];
	for(int i=0; i<ilosc;i++) {
		do {
			numery[i]=new Random().nextInt(maksimum-minimum+1)+minimum; //losuje liczby w zadanym przedziale <minimum - maksimum>
			for (int j=0; j<i;j++) {
				if (numery[j]==numery[i]) {
					numery[i]=-1;
					// System.out.println("Duplikat!!!"); // pokazuje ile razy wystapil duplikat
				}				
			}
		}while (numery[i]==-1);
		if(i!=ilosc-1) {    
			System.out.print(numery[i]+", ");
		}else {System.out.print(numery[i]); }
	}
	}
//*******************************************************************************************	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int choose =0;
		String greet ="\n--------------------------------"
				+ "\n Witaj w programie losowań\n" //greet - przywitanie
				+ "Wybierz jedna z podanych opcji\n\n"
				+ "1. Losowanie lotto\n2. Losowanie multilotek\n"
				+ "3. Losowanie szczęsliwy numerek\n4. Ekstra pensja\n"
				+ "99.Wyjście\n--------------------------------\nTwój wybór: ";
		while (choose != 99) {
		System.out.println(greet);
		Scanner input = new Scanner(System.in);
		choose = input.nextInt();
		if (choose==99) {
		input.close(); //zamknięcie wejscia  
		}
		switch (choose) {
		case 99: return;
		case 1: System.out.println("Twoj zestaw na Lotto! ");
			losPrzedzialBezDuplikatow(6, 46, 1);
			break;
		case 2: System.out.println("Twoj zestaw na Multilotek!");
			losPrzedzialBezDuplikatow(20, 80, 1);
			break;
		case 3: System.out.println("Twoj zestaw na Szczęsliwy Numerek!"); 
			losPrzedzialBezDuplikatow(4, 45, 1);
			System.out.println("\nTwój szczęśliwy numerek!");
			System.out.println(losPrzedzial(1,36));
			break;
		case 4: System.out.println("Twoj zestaw na Ekstra Pensję!"); 
			losPrzedzialBezDuplikatow(4, 35, 1);
			System.out.println("\nDodatkowy numerek!");
			System.out.println(losPrzedzial(1,4));
			break;
		default: System.out.println("Twoj wybor jest niestandardowy!");
		}
		
		}
						
	}

}
