/*
 * DO ZROBIENIA:
 * PROGRAM + ALGORYTM
 *  
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */
	//rownania testowe
	// 3+4*2/(1-5)^2 = 3.5   ->ONP: 3 4 2 * 1 5 - 2 ^ / +	wynik: 3.5
	// 12 + 1 * (2 * 3 + 4 / 5) = 18.8  ->ONP: 12 1 2 3 * 4 5 / + * +  wynik: 18.8
	// (2-5)^2*3*(7+2)*(3-5)*2 = -972  ->ONP: 2 5 - 2 ^ 3 * 7 2 + * 3 5 - * 2 * wynik: -972

import java.util.Scanner;

/**
 * Program do obliczania i konwersji RPN (Reverse Polish Notation) 
 * ONP (Odwrotna Notacja Polska) 
 * @author adam.kusmierski -> adakus@linux.pl
 *
 */
public class Main {
	
	static String wyrazenie="";
	
	
	public static void main(String[] args) {
			new Main().menu();			 
	}
	
//---------------------------------------------------------
	/**
	 * 
	 * Oblicza wyrażenia ONP - RNP. Wyrażenie dla prawidłowego działania metody 
	 * musi mieć liczby (cyfry) i operatory pooddzielane spacjami.
	 * np. 2 3 5 + /
	 * 
	 */
	void obliczONP() {
		String tmp[]=wyrazenie.trim().split(" ");
		StosTablica stos = new StosTablica(tmp.length);// Dynamiczna deklaracja wielkosci stosu
		try {
		for (String element: tmp) {
			if(element.matches("\\d|[-]?[0-9][.]?\\d*")) {
				stos.odlozNaStos(element);
			}else {
                double liczba2 = Double.valueOf(stos.wezZeStosu());
                double liczba1 = Double.valueOf(stos.wezZeStosu());

                switch(element) {
                    case ("+"):
                    	stos.odlozNaStos(Double.toString(liczba1 + liczba2));
                   
                        break;
                    case ("-"):
                    	stos.odlozNaStos(Double.toString(liczba1 - liczba2));
                        break;
                    case ("*"):
                    	stos.odlozNaStos(Double.toString(liczba1 * liczba2));
                        break;
                    case ("/"):
                    	stos.odlozNaStos(Double.toString(liczba1 / liczba2));
                        break;
                    case ("^"):
                    	stos.odlozNaStos(Double.toString(Math.pow(liczba1, liczba2)));
                        break;
                    default:
                        println("Niewłaściwy operator w wyrażeniu !!");
                        return;
                }
            }
        }
		}catch(Exception e) {
			println("Podałeś niewłaściwe wyrażenie, spróbuj jeszcze raz !!!");
			menu();
			
		}

        if(!stos.stosJestPusty())
            println("wynik: " + stos.wezZeStosu()); // wyswietla wynik działania RNP-ONP
			
		}
//---------------------------------------------------------	
	/**
	 * Sterowanie programem. Operator podejmuje decyzję co chce zrobić.
	 * Można uruchomić kalkulator ONP - RNP i podać wyrazenie w ONP,
	 * lub wybrać konwerter i zmienić wyrażenie arytmetyczne na ONP - RNP
	 * -Wybór konwertera powoduje automatyczne wywołanie kalkulatora ONP i 
	 * obliczenie wartości wyrażenia przekonwertowanego do ONP.
	 */
	void menu() {
		String decyzja = "";
		do {
			println("1. Oblicz ONP-RNP\n"
				+ "2. Przekształć wyrażenie prefix na ONP i oblicz\n"
				+ "q. Koniec Programu");
			decyzja = wczytajZeSkanera();
			switch(decyzja){
			case("1"):
				wczytajWyrazenieONP(decyzja);
				obliczONP();
				break;
			case("2"):
				wczytajWyrazenieArytm();
				wczytajWyrazenieONP(decyzja);
				obliczONP(); 
				break;
			case("q"):
				println("Zamykam program !!!");
				System.exit(0);
			default:
				println("Zły wybór !!!");
			}
		}while(!decyzja.equals("q"));
		
	}
//---------------------------------------------------------
	// test  3-2 ok, -2+3 ok, -2.0+3.0 ok, 2.0-3.0 ok (-2+3)*5 ok,
	// 			-2+(-2+3)*2*(-2+1) ok, (2.5-5)^2*3.2*(7+2.3)*(3-5)*0.2=-74.4 ok
	// 			(2.5-5)^2*3.2*(7+2.3)*(3-5)^0.2=NaN ok-pierwiastek z liczby ujemnej
	//			(3-5)*(-0.2) ok, (2.5-1.5)^0.2*3.2*(7+2.3)*(3-5)*0.2=-11.904000000000003
	/**
	 * Wczytuje wyrażenie arytmetyczne i wstępnie je oczyszcza ze zbędnych 
	 * znaków specjalnych i liter. Jednak wyrażenie powinno być poprawne 
	 * arytmetycznie i nie zawierać zwielokrotnionych znaków działań matematycznych.
	 */
	void wczytajWyrazenieArytm(){
		println("Podaj prawidłowe wyrażenie arytmetyczne do przekształcenia\n"
				+ "używając tylko cyfr i operatorów: +, -, *, /, ^\n"
				+ "oraz '.' jako separatora dziesiętnego i liczb ujemnych w nawiasach np (-1.3)\n"
				+ "Przykładowe wyrażenie: (2-5)^2*3*(7+2)*(3-5)*(-0.2)");
		wyrazenie=wczytajZeSkanera();
		wyrazenie=wyrazenie.replaceAll("[\\\\#!@%`_=~{}\\[\\]\\?<>,|]?", "");
		wyrazenie=wyrazenie.replaceAll("[a-zA-Z]?", "");
		wyrazenie=wyrazenie.replaceAll(" ", "");
		try {
		if (wyrazenie.matches("[0-9[+][-][*][/][\\^][(][)][.]?]*")){
					println("Podałeś rownanie: "+ wyrazenie);
			odczytajElementyWyrArt();
		}else {
			println("Zle rownanie !!!");
		}
		}catch(Exception e)	{
			println("Podałeś nieprawidłowe wyrażenie, spróbuj jeszcze raz!!!"+e);
			menu();
		}
	}
//---------------------------------------------------------
	/**
	 * Ustalenie priorytetu znaku odczytanego.
	 * @param znak jako string "+", "-", "*", "/", "^" 
	 * @return integer - priorytet znaku arytmetycznego
	 * np:
	 * priorytet = 0  ->  '('; nieznany znak
	 * priorytet = 1  ->  '+'; '-'; ')' 
	 * priorytet = 2  ->  '*'; '/'
	 * priorytet = 3  ->  '^'
	 */
	int priorytet(String znak) {
	
		if (znak.equals("(")) {
			return 0;
		}else if(znak.equals("+")||znak.equals("-")||znak.equals(")")) {
			return 1;
		}else if(znak.equals("*")||znak.equals("/")) {
			return 2;
		}else if(znak.equals("^")) {
			return 3;
		}else {
			return 0;
		}
		
	}
//---------------------------------------------------------	
/**
 * Odczytuje podane wyrażenie arytmetyczne element po elemencie i rozdziela 
 * na stos lub wyjscie zgodnie z zależnością:
 * ->  Jeśli odczytany element jest cyfrą bądz liczbą to wyślij na wyjscie.
 * ->  +, -, *, /, %, ^. Jeżeli priorytet operatora wczytywanego jest wyższy od priorytetu 
 * operatora znajdującego się w wierzchołku stosu lub stos jest pusty, to dopisz do stosu 
 * operator, w przeciwnym razie odczytaj i prześlij na wyjście kolejne operatory z 
 * wierzchołka stosu o priorytecie większym lub równym priorytetowi wczytanego operatora, 
 * po czym wpisz do stosu operator.	
 * ->  Gdy znaleziony element ')' to odczytuje ze stosu i przesyła na wyjście wszystkie operatory aż do 
 * nawiasu '(', który należy odczytać, ale nie wysyłać na wyjście.
 * ->  Na koniec, gdy wszystkie elementy zostały wczytane należy zdjąć wszystkie
 *  operatory ze stosu i przesłać je na wyjście.
 */
	void odczytajElementyWyrArt() {

		Stos stos=new StosTablica(wyrazenie.length());
		Wyjscie wyj = new WyjscieTablica(wyrazenie.length());
		String temp="";
		String tmp="";
		
		for(int i = 0; i < wyrazenie.length(); i++) {
			temp=wyrazenie.substring(i, i+1);
			// "\\d|[-]?[1-9].\\d*"
			// "[0-9[-]?[.]?]*"
			if(temp.matches("[0-9[-]?[.]?]*")&&i==0||temp.matches("[0-9[.]?]*")&&i!=0||
					temp.matches("[0-9[-]?[.]?]*")&&i!=0&&wyrazenie.substring(i-1, i).equals("(")) {
				//dzieki temu liczb nie dzieli na cyfry !!!
				if(i!=wyrazenie.length()-1) { // pomija wyszukiwanie liczb jesli ostatni element stringu
					if(wyrazenie.substring(i+1, i+2).matches("[0-9[.]?]*")) {
						tmp=tmp+temp;
					}else {
						temp=tmp+temp;
						wyj.odlozNaWyjscie(temp);
						tmp="";
					}
				}else if(!tmp.equals("")&&i==wyrazenie.length()-1) {
					temp=tmp+temp;
					wyj.odlozNaWyjscie(temp);
				}else {
					wyj.odlozNaWyjscie(temp);
				}
			}else {				
				if (temp.equals("(")) {
					stos.odlozNaStos(temp);
				}else if(temp.equals("+")||temp.equals("-")||temp.equals("*")
						||temp.equals("/")||temp.equals("^")) {
					if(priorytet(temp) > priorytet(stos.sprawdzSzczytStosu())||stos.stosJestPusty()) {
						stos.odlozNaStos(temp);
					}else {
						while(priorytet(stos.sprawdzSzczytStosu()) >= priorytet(temp))
							wyj.odlozNaWyjscie(stos.wezZeStosu());
						stos.odlozNaStos(temp);
					}
				}else if(temp.equals(")")) {
					while(!stos.sprawdzSzczytStosu().equals("(")){
						wyj.odlozNaWyjscie(stos.wezZeStosu());
					}
					stos.wezZeStosu(); // zapomnij o znaku '(' i ')'
					
				}else {
					println("Nieznzny znak !!!");
					
				}
			}
		}
		while(!stos.stosJestPusty()) 
			wyj.odlozNaWyjscie(stos.wezZeStosu());
		println(wyrazenie = wyj.wyswietlWyjscie());
	}
//---------------------------------------------------------
	
	@SuppressWarnings("resource")
	static String wczytajZeSkanera() {
		try {		
			Scanner skaner = new Scanner(System.in);
			return skaner.nextLine();
		}catch (Exception e) {
			return ("Coś poszło nie tak, spróbuj jeszcze raz ! ");
		}
	}
//---------------------------------------------------------
	/**
	 * Jeśli wyrażenie nie było przekształcane na ONP. 
	 * Metoda wywołuje metodę 'wczytajZeSkanera()'. Wyrażenie nie jest 
	 * sprawdzane pod kątem poprawności ONP - musi więc być wpisane prawidłowo.
	 * Elementy (liczby i znaki) w wyrażeniu muszą być oddzielone spacjami.
	 * np. '2 3 4 * *'
	 */
	void wczytajWyrazenieONP(String decyzja){
		if(!decyzja.equals("2")) {
			println("Podaj prawidłowe wyrażenie RNP-ONP oddzielając elementy spacją!");
			wyrazenie=wczytajZeSkanera();		
		}else {
			println("Obliczam wyrażenie arytmetyczne przekształcone do ONP");
		}
	}
//---------------------------------------------------------
	/**
	 * Metoda drukuje podan string i przenosi do nowej lini. w nowej lini 
	 * drukuje podkreslenie "---".
	 * @param opis - to zostanie wydrukowane.
	 */
	void println(String opis) {
		System.out.println(opis);
		System.out.println("---");
	}
//---------------------------------------------------------
	
}
