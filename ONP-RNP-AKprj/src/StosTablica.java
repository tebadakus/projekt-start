
public class StosTablica implements Stos {

	private int maxRozmiarStosu;
    private String[] stosTablica; 
    private int szczytStosu;
	
    
    public StosTablica(int length) {  // Konstruktor
    	this.maxRozmiarStosu=length;
    	stosTablica = new String[maxRozmiarStosu];
        szczytStosu = -1;
	}

	@Override
	public void odlozNaStos(String element) {
		stosTablica[++szczytStosu]=element;
		
	}

	@Override
	public String wezZeStosu() {
		return stosTablica[szczytStosu--];
	}

	@Override
	public boolean stosJestPusty() {
		if (szczytStosu==-1)	
			return true;
		return false;
	}

	@Override
	public String sprawdzSzczytStosu() {
		if(szczytStosu==-1)
			return "0";
		return stosTablica[szczytStosu];
	}

	public void setSzczytStosu(int szczytStosu) {
		this.szczytStosu = szczytStosu;
	}

}
