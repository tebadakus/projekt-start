
public interface Wyjscie {
	 
		void odlozNaWyjscie(String element);
	    String wezZWyjscia();
	    boolean wyjscieJestPuste();
	    String wyswietlWyjscie();
}
