
public class WyjscieTablica implements Wyjscie {

	private int maxRozmiarWyjscia;
    private String[] wyjscieTablica; 
    private int indexWyjscia;
	
    public WyjscieTablica(int length) {  //konstruktor
    	this.maxRozmiarWyjscia=length;
    	wyjscieTablica = new String[maxRozmiarWyjscia];
    	indexWyjscia = -1;
    	
	}
	@Override
	public void odlozNaWyjscie(String element) {
		wyjscieTablica[++indexWyjscia]=element;
	}

	@Override
	public String wezZWyjscia() {
		return wyjscieTablica[indexWyjscia--];
		
	}

	@Override
	public boolean wyjscieJestPuste() {
		if (indexWyjscia==-1)	
			return true;
		return false;
	}

	@Override
	public String wyswietlWyjscie() {
		String wzorONP = "";
		for(String element:wyjscieTablica) {
			if(element!=null)
			wzorONP=wzorONP+element+" ";
		}
		return wzorONP;
	}
}
