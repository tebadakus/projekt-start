/*
 * DO ZROBIENIA:
 * PROGRAM + ALGORYTM
 *  
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */
import java.util.Scanner;

public class Main {
	
	static String wyrazenie="";
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		do {
			new Main().wczytajWyrazenieONP();
		}while(!wyrazenie.equals("q"));
	}
	
//---------------------------------------------------------
	void obliczONP() {
		String tmp[]=wyrazenie.trim().split(" ");
		Stos stos = new StosTablica(tmp.length-1);// Dynamiczna deklaracja wielkosci stosu
		
		for (String element: tmp) {
		//	if(element.contains("\\d|[-]?[1-9]\\d*")) { //sprawdzić
			if(element.matches("\\d|[-]?[1-9]\\d*")) {
				double liczba = Double.valueOf(element);
				stos.odlozNaStos(liczba);
				
			}else {
				
			
			
                double liczba1 = stos.wezZeStosu();
                double liczba2 = stos.wezZeStosu();

                switch(element) {
                    case ("+"):
                    	stos.odlozNaStos(liczba1 + liczba2);
                   
                        break;
                    case ("-"):
                    	stos.odlozNaStos(liczba1 - liczba2);
                        break;
                    case ("*"):
                    	stos.odlozNaStos(liczba1 * liczba2);
                        break;
                    case ("/"):
                    	stos.odlozNaStos(liczba1 / liczba2);
                        break;
                    default:
                        println("Niewłaściwy operator w wyrażeniu !!");
                        return;
                }
            }
        }

        if(!stos.stosJestPusty())
            println("wynik: " + stos.wezZeStosu());
			
		}
	
//---------------------------------------------------------
	@SuppressWarnings("unused")
	void wczytajWyrazenieONP(){
		println("Podaj wyrażenie oddzielając elementy spacją!");
		try {
			Scanner skaner = new Scanner(System.in);
			println(wyrazenie=skaner.nextLine());
			obliczONP();
		}catch (Exception e) {
			println("Coś poszło nie tak, spróbuj jeszcze raz ! "+ e);
			// TODO: handle exception
		}
		
	}
//---------------------------------------------------------
	void println(String opis) {
		System.out.println(opis);
		System.out.println("---");
	}
//---------------------------------------------------------
	
}
