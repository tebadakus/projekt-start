
public class StosTablica implements Stos {

	private int maxRozmiarStosu;
    private double[] stosTablica; 
    private int szczytStosu;
	
    public StosTablica(int length) {
		// TODO Auto-generated constructor stub
    	this.maxRozmiarStosu=length;
    	stosTablica = new double[maxRozmiarStosu];
        szczytStosu = -1;
    	
	}

	@Override
	public void odlozNaStos(double element) {
		// TODO Auto-generated method stub
		
		stosTablica[++szczytStosu]=element;
		//szczytStosu++;
	}

	@Override
	public double wezZeStosu() {
		// TODO Auto-generated method stub
		return stosTablica[szczytStosu--];
		//szczytStosu--;
	}

	@Override
	public boolean stosJestPusty() {
		// TODO Auto-generated method stub
		if (szczytStosu==-1)	
			return true;
		return false;
	}

	@Override
	public boolean stosJestPelny() {
		// TODO Auto-generated method stub
		if (szczytStosu==maxRozmiarStosu-1)
			return true;
		return false;
	}

}
