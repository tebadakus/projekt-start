package obslugaBledow;



	/*
	 * Zadanie:
	 * - poprzednio tworzony kalkulator wzbogacic o obsluge wyjatkow/bledow
	 * - dodac funkcje, ktore beda mialy za zadanie wczytywac wartosci do kalkulatora 
	 * na wzor kodu ponizej (czyli wczytujaca jedynie wartosci typu float i zadne inne)
	 * - przy dzieleniu takze zastapic potencjalny if na try - catch
	 * - dodatkowo w przypadku wyjatku dzielenia przez zero sprawdzic funkcjonowanie klauzuli
	 * finally; przyklad:
	 * try {
	 * 		//kod
	 * }
	 * catch (Exception e) {
	 * 		//obsluga bledu, np. jakis komunikat
	 * }
	 * finally {
	 * 	    //kod - wlasnie, jak on sie wykona?
	 * }
	 */

	import java.util.Scanner;

	public class WyjatkiTEB {

		public static double zmiennaGlobal=1.0; 
		
		static int wczytajInt() {
			int a,b;
			Scanner scan = new Scanner(System.in);
			try {
				return scan.nextInt();
			}
			catch (Exception e) {
				System.err.println("Wpisałes wartosc inna niz int!");
			}
			System.out.println(zmiennaGlobal);
			return -1;
		}
		
		public static void main(String[] args) {
			
			int a=-1,b;
			System.out.println("Podaj pierwsza liczbe: ");
			while(a<0) {
				a=wczytajInt();
			}
			System.out.println(zmiennaGlobal);
			System.out.println("Koniec programu");
		}
	}




