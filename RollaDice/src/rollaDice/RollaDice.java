package rollaDice;

import java.util.Random;
import java.util.Scanner;

public class RollaDice {

	//****************************************************************
		static int randInNumberRange(int min, int max) {
			int rand=new Random().nextInt(max-min+1)+min;
			return rand;
		}
	//****************************************************************
	
		static void showDice(int drawn) {
			switch(drawn){
			case 1:
				System.out.println("-----");
				System.out.println("     ");
				System.out.println("  *  ");
				System.out.println("     ");
				System.out.println("-----");
				break;
			case 2:
				System.out.println("-----");
				System.out.println("  *  ");
				System.out.println("     ");
				System.out.println("  *  ");
				System.out.println("-----");
				break;
			case 3:
				System.out.println("-----");
				System.out.println("  *  ");
				System.out.println("  *  ");
				System.out.println("  *  ");
				System.out.println("-----");
				break;
			case 4: 
				System.out.println("-----");
				System.out.println(" * * ");
				System.out.println("     ");
				System.out.println(" * * ");
				System.out.println("-----");
				break;
			case 5:
				System.out.println("-----");
				System.out.println(" * * ");
				System.out.println("  *  ");
				System.out.println(" * * ");
				System.out.println("-----");
				break;
			case 6:
				System.out.println("-----");
				System.out.println(" * * ");
				System.out.println(" * * ");
				System.out.println(" * * ");
				System.out.println("-----");
				break;
			default:
				System.out.println(drawn);
			}
		}
	//****************************************************************	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String choose;
		do { 
			showDice(randInNumberRange(1, 6));
			
			System.out.println("-------------------\nq- Quite\nr- roll a dice");
			Scanner input= new Scanner(System.in);
		    choose = input.next();
			if (choose.equals("q")){
				input.close();
			}
			
		} while(!choose.equals("q"));
	}
}
