package kalkulatorTEB;

/*
 * Zadanie:
 * - poprzednio tworzony kalkulator wzbogacic o obsluge wyjatkow/bledow
 * - dodac funkcje, ktore beda mialy za zadanie wczytywac wartosci do kalkulatora 
 * na wzor kodu ponizej (czyli wczytujaca jedynie wartosci typu float i zadne inne)
 * - przy dzieleniu takze zastapic potencjalny if na try - catch
 * - dodatkowo w przypadku wyjatku dzielenia przez zero sprawdzic funkcjonowanie klauzuli
 * finally; przyklad:
 * try {
 * 		//kod
 * }
 * catch (Exception e) {
 * 		//obsluga bledu, np. jakis komunikat
 * }
 * finally {
 * 	    //kod - wlasnie, jak on sie wykona?
 * }
 * 
 * ZADANIE 2:
 * - przetowrzyc zmienne na float
 * - doprowadzic do momentu by menu dzialalo do chwili az uzytkownik nie wpisze liczby zaknaczajacej
 * dzialanie programu (mozna wykorzystac rekurencje)
 * - stworzyc potegowanie za pomoca rekurencji
 * - zastanowic sie (i najlepiej wykonac) w jaki sposob wysietlic uzytkownik wszystkie podane
 * przez niego liczby, z ktorych wyswietlamy mu wynik (np. wyswietlic mu 10 liczb ktore wprowadzil)
 * - obsluzyc wszelkie bledy wprowadzania (np. podales zero albo podales wartosc ujemna - szczegolnie
 * tyczy sie silni)
 * - zobaczyc czym jest i wprowadzic modulo (i sprawdzic czy/czemu nie dziala na zmiennoprzecinkowych).
 */

import java.util.Scanner;

public class KalkPDobTEBwyswietlanie {

	
	static int wczytajInt() {
		return wczytajInt("");
	}
	
	static int wczytajInt(String tekstWejsciowy) {
		
		/*
		 //tekst wyswietla sie tylko wtedy gdy zmienna tekstWejsciowy nie jest pusta
		 //wynik warunku zapisanego w nawiasach okrągłych MUSI być prawdą (true) by
		 //wykonały sie instrukcje przypisane tego do warunku
		 //podany przed funkcją wykrzyknik powoduje, że wartość uzyskana z tej funkcji
		 //będzie odwrócona czyli jeżeli zwrócona wartość była prawdą to otrzymamy fałsz
		 //jeżeli zwrócony był fałsz to otrzymamy prawdę
		 //w poniższym przykładzie jeżeli użytkownik nie podał żadnego ciągu znakowego
		 //(jego długość wynosi 0 czyli żadnej litery) to isEmpty() zwróci prawdę;
	  	 //w związku z tym nasz kod zachował by się nie tak jak byśmy sobie tego życzyli -
		 //wyświetlałby pusty ciąg znakowy; stąd użycie negacji (wykrzyknik) by tekst
		 //wyświetlał się jedynie w przypadku, gdy użytkownik jednak coś wpisał.
		*/
		
		if (!tekstWejsciowy.isEmpty())
			print(tekstWejsciowy);
		try {
			return Integer.parseInt(wczytajCiag()); //wczytajCiag() funkcja zawierajaca tylko Scanner do wczytywania.
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int! "+e);
		}
		return 0;
	}
	
	//analogicznie do wczytajInt sporzadzone sa funkcje dla zmiennoprzecinkowych
	//**************************************************************************************************
	static double wczytajDouble() {
		return wczytajDouble("");
	}
	
	static double wczytajDouble(String prompt) {
		if (!prompt.isEmpty())
			print(prompt);
		try {
			return Double.parseDouble(wczytajCiag());
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz Double!");
			
		}
		return wczytajDouble("Podaj ponownie: ");// dziala dla modulo sprawdz jak dziala z innymi
		//return 0; // ten return byl wykozystywany poza funkcja do ponownego wczytania gdy wystapil blad
	}
//***************************************************************************************	
	@SuppressWarnings("resource") //zapobiega informacji o nie zamknieciu Scannera.
	static String wczytajCiag() {
		return new Scanner(System.in).nextLine();
	}
//***************************************************************************************	
	static int wczytajUInt() {
		@SuppressWarnings("resource")
		int ret = 0;
		try {
			ret = Integer.parseInt(wczytajCiag());
			//to informacja, nie blad!
			if (ret<0) {
				println("Podana wartosc jest mniejsza od zera!"); 
				//println("Wpisz jeszcze raz: ");
				return 0;}
			return ret;
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int!  "+e);
			//println("Wpisz jeszcze raz: ");
		}
		return 0;
	}
//*****************************************************************************************	
	static void print(Object o) {
		System.out.print(o);
	}
	
	static void println(Object o) {
		System.out.println(o);
	}
//****************************************************	
	static int iloscOperacji() {
		return iloscOperacji("");
	}
//****************************************************	
	static int iloscOperacji(String text) {
		int ilosc=0;
		if (text.isEmpty())
			println("Podaj na ilu wartosciach chcesz dokonac wybranej operacji. ");
		else
			print(text);
		do {print("Wpisz wartość: ");
			ilosc=wczytajUInt();}
		while (ilosc==0);
		return ilosc;
	}
//***************************************************************************	
	static void menu() {
		System.out.println("Kalkulator w Java");
		System.out.println("Wybierz jedna z operacji:");
		System.out.println("1. Dodawanie\n2. Odejmowanie"
				+ "\n3. Mnożenie\n4. Dzielenie"
				+ "\n5. Potegowanie\n6. Pierwiastkowanie"
				+ "\n7. Pierwiastkowanie wg Newtona"
				+ "\n8. Silnia\n9. Modulo\n99. Wyjscie");
		System.out.print("Wybor: ");

		int wybor;
		do {
			wybor=wczytajUInt();
		} while(wybor==0);
		
	    switch(wybor) {
		    case 1: dodawanie(iloscOperacji()); break;
		    case 2: odejmowanie(iloscOperacji()); break;
		    case 3: mnozenie(iloscOperacji()); break;
		    case 4: dzielenie(iloscOperacji()); break;
		    case 5: potegowanie(); break;
		    case 6:pierwiastkowanie(wczytajDouble("Podaj podstawę pierwiastka: "), wczytajInt("Podaj stopień pierwiastka: ")); break;
		    case 7: pierwiastkowanieReal(iloscOperacji("Podaj podstawę pierwiastka: "),wczytajInt("Podaj stopień pierwiastka: ")); break;
		    case 8: silnia(iloscOperacji()); break;
		    case 9: moduloDzielenie(wczytajDouble("Podaj narzędnik: "),wczytajDouble("Podaj mianownik: "));	break;
		    case 99: println("Dziękujemy za skorzystanie z naszego programu");return;
		    default: println("Nie wybrales poprawnie operacji, sprobuj ponownie!");
	    }
	    println("Naciśnij cokolwiek by kontynuowa");
	    wczytajCiag();
	    println("------------------------------------------------");
	    menu();
	}
	
	
	
//******************************************************************************	
	static void dodawanie(int ilosc) {
		double skladniki[] = new double[ilosc];
		double suma=0;
		
		for (int i=0;i<ilosc;i++) {
			
			do 	skladniki[i]=wczytajDouble("Podaj liczbęróżną od 0: "); 
			while(skladniki[i]==0);
			suma+=skladniki[i];
		}
		print("Suma podanych liczb: ");
		for (int i=0;i<ilosc;i++) {
			if (skladniki[i]<0)		print("(" + skladniki[i] + ")");
			else	print(skladniki[i]);
			if (i!=ilosc-1)		print(" + ");
		}
		println(" = " + suma);
	}

//******************************************************************************
	static void odejmowanie(int ilosc) {
		double skladniki[] = new double[ilosc];
		double wynik=0;
		while(ilosc-->0) {
			//print("Podaj liczbę różną od 0: ");
			if((skladniki[ilosc]=wczytajDouble("Podaj liczbęróżną od 0: "))==0)
				ilosc++;
			else	wynik-=skladniki[ilosc];
		}
		print("Różnica podanych liczb: ");
		/*
		 *po pierwsze nasza funkcja tylko ODEJMUJE, co za tym idzie każda wartość podana
		 *przez użytkownika jest odejmowana od liczby 0; co za tym idzie powinniśmy 
		 *poinformować naszego użytkownika, że wprowadzona przez niego wartość jest
		 *tak naprawdę ODEJMOWANA od zera. Do pierwszej wprowadzonej liczy dodatniej dopisujemy
		 * "-" aby użytkownik widział że wszystkie liczby sa odejmowane od zera.
		*/
		if (skladniki[skladniki.length-1]>0)
			print("-");
		/*
		 //jeżeli natomiast użytkownik wprowadził wartość ujemną - musimy ją przemienić
		 //podczas wyświetlania na dodatnią (żeby się nie dziwił nierealnym 
		 //przedstawieniem różnicy)
		*/
		else	skladniki[skladniki.length-1]*=-1;
		
		/*
		 //ponieważ użyliśmy funkcji while i zapisaliśmy wartości podawane przez użytkownika
		 //w odwrotnej kolejności toteż pętlę for rozpoczniemy odwrotnie - z pełną wartością
		 //długości tablicy (nasza ilosc w tecj chwili wynosi 0 i nie jest nam już przydatna)
		 //i pętla w zamierzeniu ma działać aż wartość i osiągnie 0 (z zerem włącznie);
		 //zamiast zwiększać wartość i to ją każdorazowo zmniejszamy
		*/
		for (int i=skladniki.length-1;i>=0;i--) {
			
			if (skladniki[i]<0)	print("(" + skladniki[i] + ")");
			else	print(skladniki[i]);
			if (i!=0)	print(" - ");
		}
		println(" = " + wynik);
	}
	
//**************************************************************************************	
	static void mnozenie(int ilosc) {
		double skladniki[]=new double[ilosc];
		double wynik=1;
		while(ilosc-->0) {
			//print("Podaj liczbęróżną od 0: ");
			if((skladniki[ilosc]=wczytajDouble("Podaj liczbęróżną od 0: "))==0)
				ilosc++;
			else wynik*=skladniki[ilosc];
		}
		print("Iloraz podanych liczb: ");
		
		int i=skladniki.length;
		while(i-->0) {
			if(skladniki[i]<0) print("("+skladniki[i]+")");
			else print(skladniki[i]);
			if(i!=0) print(" * ");
		}
		println(" = "+wynik);
	}	
//*************************************************************************************
	
	static void dzielenie(int ilosc) {
		double skladniki[]=new double[ilosc+1];
		double wynik;
		do {	wynik=wczytajDouble("Podaj pierwszą wartość do dzielenia: ");
			skladniki[ilosc]=wynik;	}
		while (wynik==0);	
		while(ilosc-->0) {
			//double tymczasowa;
			if((skladniki[ilosc]=wczytajDouble("Podaj wartość przez, którą podzielisz aktualny wynik: "))==0)
				ilosc++;
			else	wynik/=skladniki[ilosc];
		}
		print("Wynik dzielenia podanych liczb: ");
		
		int i=skladniki.length;
		while(i-->0) {
			if(skladniki[i]<0) print("("+skladniki[i]+")");
			else print(skladniki[i]);
			if (i!=0) print(" / ");
		}
		println(" = "+wynik);
	}
	
	//*******************************************************************************
	static void potegowanie() {
		double p, n;
		do
			n=wczytajInt("Podaj wartość potęgi: ");
		while(n==0);
		do
			p=wczytajDouble("Podaj liczbę do potęgowania: ");
		while(p==0);
		println("Wynik potegowania: " + p + "^" + (int)n + " = " + potegowanie(n,p));
	}
	
	static double potegowanie(double potega) {
		return potegowanie(potega, potega);
	}
	
	static double potegowanie(double potega, double podstawa) {
		if (potega<0) return 1/nPotegowanie(potega,podstawa);
		if (potega==0)
			return 1;
		if (potega<2) 
			return podstawa;
		return podstawa*potegowanie(potega-1,podstawa);
	}
	
	static double nPotegowanie(double potega, double podstawa) {
		if (potega>-2) 
			return podstawa;
		return podstawa*nPotegowanie(potega+1,podstawa);
	}
	//********************************************************************************
	/*
	 * Działanie funkcji na przykładzie 2^4
	 * 1. Zmienna potega=4, podstawa=2 
	 * Funkcja sprawdza potęgę;ponieważ nie wynosi 1 oraz nie jest mniejsza od 2
	 * Wykonuje się  kod
	 * 		zwróć podstawa*potegowanie(potega-1,podstawa);
	 * podstawa mnożona jest przez wynik samowywołanej funkcji; ponieważ nie możemy na tym 
	 * miejscu ustalić zwracanej wartości, prorgram wykonuje ponownie funkcję potegowanie
	 * z nowymi parametrami
	 * 
	 * 2. Teraz zmienna potega=3, podstawa=2
	 * Funkcja ponownie sprawdza, czy potega jest rowna 0 lub czy jest mniejsza od 2
	 * Ponieważ nie jest sytuacja się powtarza -> ponownie wykonujemy funkcję potęgowanie
	 * z nowymi parametrami
	 * 
	 * 3. Teraz zmienna potega=2, podstawa=2
	 * Punkt analogiczny do 2
	 * 
	 * 4. Teraz zmienna potega=1, podstawa=2
	 * W tym miejscu potega nadal nie jest równa 0 ale jest mniejsza od 2 (1) więc funkcja 
	 * zwraca wartość 2 i kończy swoje działanie (nie wywołuje kolejnej samej siebie)
	 * 
	 * 5. Wynik z punktu 4 pobierany jest do funkcji wywołanej w punkcie 3. Oznacza to,
	 * że podstawa została pomnożona przez wartość 2 (2*2=4)
	 * 
	 * 6. Wynik z punktu 5 (wartość równa 4) zostaje pomnożony na wyjściu funkcji z punktu 2. 
	 * Wynik to 4*2=8
	 * 
	 * 7. Działanie wraca do punktu 1. Mamy już wyniki z poprzednio wywołanych funkcji 
	 * (z samowywołania) i pozyskaną wartość mnożymy przez podstawę; 8*2=16; tyle też 
	 * wynosi wynik równania 2^4=16
	 */
	
	static void pierwiastkowanie(double ilosc) {
		pierwiastkowanie(ilosc,2);
	}
	//pieriwatkowanie działające dla wartości całkowitych, zle radzi sobie z 
	//ułamkami
	static void pierwiastkowanie(double podstawa, double stopien) {
		//boolean koniec=false;
		double wynik=0;//(int)podstawa/(int)stopien;
		while(true) {
			wynik=(int)wynik/(int)stopien;
			if(potegowanie(stopien,wynik)<podstawa) 
				break;
		}
		if (potegowanie(stopien,wynik)==podstawa) {
			println("Pierwiastek z podanej liczby to: " + wynik);
			return; 
		}
		
		while(true) { 
			if(potegowanie(stopien,wynik)>=podstawa)
				break;
			wynik++;
		}
		println("Pierwiastek z podanej liczby to: " + wynik);
	}
	//ta wersja pierwiastkowania jest precyzyjniejsza, korzysta z równiania Newtona
	static void pierwiastkowanieReal(double ilosc, double stopien) {
		double nastepna=0;
		double poprzednia= 1;
		double roznicaSzukana=1e-5; //1 * 10^-5 = 0,00001
		double roznica=ilosc;
		while (roznica>roznicaSzukana) {
			nastepna=((stopien-1)*poprzednia + ilosc/potegowanie(stopien-1,poprzednia))/stopien;
			roznica=Math.abs(nastepna-poprzednia);
			poprzednia=nastepna;
		}
		//dzięki tej linii wyświetlamy tylko 3 miejsca po przecinku wyniku naszego pierwiastka
		println("Pierwiastek z podanej liczby to: " + 
		stopien + (char)(251) + ilosc + " = " + String.format("%1$.3f", nastepna));
		//w nawiasie 251 to kod znaku pierwiastka - nie obsługiwany przez konsolę...+
	}
//*****************************************************************	
	static int silnia(int ilosc) {
		return silnia(ilosc, 1);
	}

	static int silnia(int ilosc, int wynik) {
		if (ilosc==0) return wynik;
		return silnia(ilosc-1,wynik*ilosc);	
	}
//*****************************************************************
	static void moduloDzielenie(double n, double m) {
		
		println("Wynik dzielenia modulo "+n+" % "+m+" = "+n%m);
	}
//*****************************************************************
	
	public static void main(String[] args) {
		
		menu();
		
	}
}
