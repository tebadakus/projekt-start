package kalkulatorTEB;

import java.util.Scanner;

public class Kalkulator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	int wybor = 0;
	while (wybor != 99 ) {
		System.out.println("----------------------------------------------\n");
		System.out.println("Kalkulator w Java");
		System.out.println("Wybierz jedną z operacji");
		System.out.println(" 1. Dodawanie\n 2. Odejmowanie\n 3. Mnozenie\n 4. Dzielenie\n "
				+ "5. Potega\n 6. Pierwiastek kwadratowy\n 7. Pierwiastek sześcienny\n "
				+ "8. Pierwiastek n stopnia\n 9. Silnia \n "
				+ "10. Dzielenie modulo\n 11. Sinus\n 12. Cosinus\n 13. Tangens\n 14. Cotangens\n "
				+ "15. Czy liczba jest parzysta\n 99. Wyjscie");
		System.out.println("Wybor: ");
		Scanner skan=new Scanner(System.in);
		 wybor=skan.nextInt();
		if(wybor==99) {
			skan.close(); // zamyka skaner
		}
		 
		if (wybor==1) {
			System.out.println("Wybrales +");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			System.out.println(a +"+"+ b +"="+ (a+b));
		}
		if (wybor==2) {
			System.out.println("Wybrales -");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			System.out.println(a +"-"+ b +"="+ (a-b));
		}
		if (wybor==3) {
			System.out.println("Wybrales *");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			System.out.println(a +"*"+ b +"="+ (a*b));
		}
		if (wybor==4) {
			System.out.println("Wybrales /");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			if (b!=0) {
				System.out.println(a +"/"+ b +"="+ (a/(b*1.0))); // zmiana na zmienne przecinkowe przemnozenie przez (1.0)
		} else {
				System.out.println("Nie można dzielić przez zero !!");
		}
		}
		if (wybor==5) {
			System.out.println("Wybrales Potege");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			System.out.println(a +"^"+ b +"="+ Math.pow(a, b));//power
		}
		if (wybor==6) {
			System.out.println("Wybrales Pierwiastek Kwadratowy (2 stopnia)");
			System.out.println("Podaj liczbe: ");
			int a=skan.nextInt();
			System.out.println("Pierwiastek kwadratowy z "+ a +"="+ Math.sqrt(a) ); //square root
		}
		if (wybor==7) {
			System.out.println("Wybrales Pierwiastek Sześcienny (3 stopnia)");
			System.out.println("Podaj liczbe: ");
			double a=skan.nextDouble();
			System.out.println("Pierwiastek sześcienny z "+ a +" = "+ Math.cbrt(a) ); //cube root 
		}
		if (wybor==8) {
			System.out.println("Wybrales Pierwiastek n stopnia");
			System.out.println("Podaj liczbe: ");
			int a=skan.nextInt();
			System.out.println("Podaj stopień pierwiastka n: ");
			int b=skan.nextInt();
			
			System.out.println("Pierwiastek stopnia "+ b +" z liczby "+ a +" wynosi:  "+ Math.pow(a, (1./b))); //power
			
		}
		if (wybor==9) {
			System.out.println("Wybrales ! silnie");
			System.out.println("Podaj liczbe: ");
			int a=skan.nextInt();
			int c = 1;
			for (int i=1; i<=a; i=i+1) {
				c=c*i;
			}
			System.out.println("Wynik Silni " + a +"!="+ c);
		}
		if (wybor==10) {
			System.out.println("Wybrales % dzielenie modulo");
			System.out.println("Podaj wartosc a: ");
			int a=skan.nextInt();
			System.out.println("Podaj wartosc b: ");
			int b=skan.nextInt();
			if (b==0) {
				System.out.println("Nie można dzielić przez zero");
			} else {
				System.out.println(a +"%"+ b +"="+ (a%(b*1.)));
			}
			
		}
		if (wybor==11) {
			System.out.println("Wybrales sinus - f podaje kat z przedzialu (-pi/2)<->(pi/2)");
			System.out.println("Podaj liczbe : ");
			double a=skan.nextDouble();
			
			System.out.println("Sinus z "+ a +" = "+ Math.asin(a) );
		}
		if (wybor==12) {
			System.out.println("Wybrales cosinus - f podaje kat z przedzialu (0)<->(pi)");
			System.out.println("Podaj liczbe: ");
			double a=skan.nextDouble();
			System.out.println("Cosinus z "+ a +" = "+ Math.acos(a) );
		}
		if (wybor==13) {
			System.out.println("Wybrales tangens - f podaje kat z przedzialu (-pi/2)<->(pi/2)");
			System.out.println("Podaj liczbe: ");
			double a=skan.nextDouble();
			System.out.println("Tangens z "+ a +" = "+ Math.atan(a) );
		}
		if (wybor==14) {
			System.out.println("Wybrales cotangens - f podaje kat z przedzialu (-pi/2)<->(pi/2)");
			System.out.println("Podaj liczbe: ");
			double a=skan.nextDouble();
			System.out.println("Cotangens z "+ a +" = "+ 1./Math.atan(a));
			
		}
		if (wybor==15) {
			System.out.println("Wybrales sprawdzenie parzystości liczby :");
			System.out.println("Podaj liczbe: ");
			double a=skan.nextDouble();
			if (a%2==0) {
			System.out.println("Liczba "+a+" jest parzysta/even " );
			}else {
			System.out.println("Liczba "+a+" jest nie parzysta/odd ");
			}
		}	
		if (wybor>15) {
			System.out.println("Koniec programu !");
		}
	}
  }
}