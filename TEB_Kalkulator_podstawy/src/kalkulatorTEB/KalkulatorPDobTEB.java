package kalkulatorTEB;



	/*
	 * Zadanie:
	 * - poprzednio tworzony kalkulator wzbogacic o obsluge wyjatkow/bledow
	 * - dodac funkcje, ktore beda mialy za zadanie wczytywac wartosci do kalkulatora 
	 * na wzor kodu ponizej (czyli wczytujaca jedynie wartosci typu float i zadne inne)
	 * - przy dzieleniu takze zastapic potencjalny if na try - catch
	 * - dodatkowo w przypadku wyjatku dzielenia przez zero sprawdzic funkcjonowanie klauzuli
	 * finally; przyklad:
	 * try {
	 * 		//kod
	 * }
	 * catch (Exception e) {
	 * 		//obsluga bledu, np. jakis komunikat
	 * }
	 * finally {
	 * 	    //kod - wlasnie, jak on sie wykona?
	 * }
	 * 
	 * ZADANIE 2:
	 * - przetowrzyc zmienne na float
	 * - doprowadzic do momentu by menu dzialalo do chwili az uzytkownik nie wpisze liczby zaknaczajacej
	 * dzialanie programu (mozna wykorzystac rekurencje)
	 * - stworzyc potegowanie za pomoca rekurencji
	 * - zastanowic sie (i najlepiej wykonac) w jaki sposob wysietlic uzytkownik wszystkie podane
	 * przez niego liczby, z ktorych wyswietlamy mu wynik (np. wyswietlic mu 10 liczb ktore wprowadzil)
	 * - obsluzyc wszelkie bledy wprowadzania (np. podales zero albo podales wartosc ujemna - szczegolnie
	 * tyczy sie silni)
	 * - zobaczyc czym jest i wprowadzic modulo (i sprawdzic czy/czemu nie dziala na zmiennoprzecinkowych).
	 */

	import java.util.Random;
	import java.util.Scanner;

	public class KalkulatorPDobTEB {

		static int wczytajInt() {
			return wczytajInt("");
		}
		
		static int wczytajInt(String tekstWejsciowy) {
			//tekst wyswietla sie tylko wtedy gdy zmienna tekstWejsciowy nie jest pusta
			if (!tekstWejsciowy.isEmpty())
				print(tekstWejsciowy);
			try {
				return Integer.parseInt(wczytajCiag());
			}
			catch (Exception e) {
				System.err.println("Wpisałes wartosc inna niz int!");
			}
			return 0;
		}
		//analogicznie do wczytajInt sporzadzone sa funkcje dla zmiennoprzecinkowych
		static double wczytajDouble() {
			return wczytajDouble("");
		}
		
		static double wczytajDouble(String prompt) {
			if (!prompt.isEmpty())
				print(prompt);
			try {
				return Double.parseDouble(wczytajCiag());
			}
			catch (Exception e) {
				System.err.println("Wpisałes wartosc inna niz Double!");
			}
			return 0;
		}
		
		@SuppressWarnings("resource")
		static String wczytajCiag() {
			return new Scanner(System.in).nextLine();
		}
		
		static int wczytajUInt() {
			@SuppressWarnings("resource")
			int ret = -1;
			try {
				ret = Integer.parseInt(wczytajCiag());
				//to informacja, nie blad!
				if (ret<0) println("Podana wartosc jest mniejsza od zera!");
				return ret;
			}
			catch (Exception e) {
				System.err.println("Wpisałes wartosc inna niz int!");
			}
			return -1;
		}
		
		static void print(Object o) {
			System.out.print(o);
		}
		
		static void println(Object o) {
			System.out.println(o);
		}
		
		static int iloscOperacji() {
			int ilosc=0;
			print("Podaj na ilu wartosciach chcesz dokonac wybranej operacji: ");
			do
				ilosc=wczytajUInt();
			while (ilosc==0);
			return ilosc;
		}
		
		static void menu() {
			System.out.println("Kalkulator w Java");
			System.out.println("Wybierz jedna z operacji:");
			System.out.println("1. Dodawanie\n2. Odejmowanie"
					+ "\n3. Mnożenie\n4. Dzielenie"
					+ "\n5. Potegowanie\n6. Pierwiastkowanie"
					+ "\n7. Silnia\n8. Wyjscie");
			System.out.print("Wybor: ");

			int wybor;
			do {
				wybor=wczytajUInt();
			} while(wybor<0);
			
		    switch(wybor) {
			    case 1: dodawanie(iloscOperacji()); break;
			    case 2: odejmowanie(iloscOperacji()); break;
			    case 3: mnozenie(iloscOperacji()); break;
			    case 4: dzielenie(iloscOperacji()); break;
			    case 5: println("Wynik potegowania: " + potegowanie(iloscOperacji(),wczytajDouble("Podaj liczbę do potęgowania: "))); break;
			    case 6: pierwiastkowanieReal(iloscOperacji(),wczytajInt("Podaj stopień pierwiastka: ")); break;
			    case 7: silnia(iloscOperacji()); break;
			    case 8: println("Dziękujemy za skorzystanie z naszego programu");return;
			    default: println("Nie wybrales poprawnie operacji, sprobuj ponownie!");
		    }
		    println("Naciśnij cokolwiek by kontynuowa");
		    wczytajCiag();
		    println("------------------------------------------------");
		    menu();
		}
		
		static void dodawanie(int ilosc) {
			double suma=0;
			double wczytana;
			//ilosc++ -> ilosc=ilosc+1;
			for (int i=0;i<ilosc;i++) {
				do 
					wczytana=wczytajDouble(); 
				while(wczytana==0);
				suma+=wczytana;
			}
			println("Suma podanych liczb to: " + suma);
		}
		
		static void odejmowanie(int ilosc) {
			double wynik=0,tmpWynik=0;
			while(ilosc-->0) {
				if((tmpWynik=wczytajDouble())==0)
					ilosc++;
				else
					wynik-=tmpWynik;
			}
			println("Wynik odejmowania podanych liczb to: " + wynik);
		}
		
		static void mnozenie(int ilosc) {
			double wynik=1;
			while(ilosc-->0) {
				double tymczasowa;
				if((tymczasowa=wczytajDouble())==0)
					ilosc++;
				else
					wynik*=tymczasowa;
			}
			println("Wynik mnozenia podanych liczb to: " + wynik);
		}
		
		static void dzielenie(int ilosc) {
			
		}
		
		static double potegowanie(double potega) {
			return potegowanie(potega, potega);
		}
		
		static double potegowanie(double potega, double podstawa) {
			if (potega==0)
				return 1;
			if (potega<2) 
				return podstawa;
			return podstawa*potegowanie(potega-1,podstawa);
		}
		/*
		 * Działanie funkcji na przykładzie 2^4
		 * 1. Zmienna potega=4, podstawa=2 
		 * Funkcja sprawdza potęgę;ponieważ nie wynosi 1 oraz nie jest mniejsza od 2
		 * Wykonuje się  kod
		 * 		zwróć podstawa*potegowanie(potega-1,podstawa);
		 * podstawa mnożona jest przez wynik samowywołanej funkcji; ponieważ nie możemy na tym 
		 * miejscu ustalić zwracanej wartości, prorgram wykonuje ponownie funkcję potegowanie
		 * z nowymi parametrami
		 * 
		 * 2. Teraz zmienna potega=3, podstawa=2
		 * Funkcja ponownie sprawdza, czy potega jest rowna 0 lub czy jest mniejsza od 2
		 * Ponieważ nie jest sytuacja się powtarza -> ponownie wykonujemy funkcję potęgowanie
		 * z nowymi parametrami
		 * 
		 * 3. Teraz zmienna potega=2, podstawa=2
		 * Punkt analogiczny do 2
		 * 
		 * 4. Teraz zmienna potega=1, podstawa=2
		 * W tym miejscu potega nadal nie jest równa 0 ale jest mniejsza od 2 (1) więc funkcja 
		 * zwraca wartość 2 i kończy swoje działanie (nie wywołuje kolejnej samej siebie)
		 * 
		 * 5. Wynik z punktu 4 pobierany jest do funkcji wywołanej w punkcie 3. Oznacza to,
		 * że podstawa została pomnożona przez wartość 2 (2*2=4)
		 * 
		 * 6. Wynik z punktu 5 (wartość równa 4) zostaje pomnożony na wyjściu funkcji z punktu 2. 
		 * Wynik to 4*2=8
		 * 
		 * 7. Działanie wraca do punktu 1. Mamy już wyniki z poprzednio wywołanych funkcji 
		 * (z samowywołania) i pozyskaną wartość mnożymy przez podstawę; 8*2=16; tyle też 
		 * wynosi wynik równania 2^4=16
		 */
		
		static void pierwiastkowanie(double ilosc) {
			pierwiastkowanie(ilosc,2);
		}
		
		static void pierwiastkowanie(double podstawa, double stopien) {
			boolean koniec=false;
			double wynik=(int)podstawa/(int)stopien;
			while(!koniec) { //rownoznaczne -> koniec == false
				if(potegowanie(stopien,wynik/stopien)>podstawa) {
					wynik=(int)wynik/(int)stopien;
				}
				else
					koniec=!koniec;
			}
			if (potegowanie(stopien,wynik)==podstawa) {
				println("Pierwiastek z podanej liczby to: " + wynik);
				return; 
			}
			koniec=!koniec;
			while(!koniec) { 
				if(potegowanie(stopien,wynik-1)>=podstawa)
					wynik--;
				else 
					break;
			}
			println("Pierwiastek z podanej liczby to: " + wynik);
		}
		
		static void pierwiastkowanieReal(double ilosc, double stopien) {
			double obecna=0;
			double start= new Random().nextInt(10);
			double roznicaSzukana=1e-5;
			double roznica=ilosc;
			while (roznica>roznicaSzukana) {
				obecna=((stopien-1)*start + ilosc/potegowanie(stopien-1,start))/stopien;
				roznica=Math.abs(obecna-start);
				start=obecna;
			}

			println("Pierwiastek z podanej liczby to: " + obecna);
		}
		
		static int silnia(int ilosc) {
			return silnia(ilosc, 1);
		}

		static int silnia(int ilosc, int wynik) {
			if (ilosc==0) return wynik;
			return silnia(ilosc-1,wynik*ilosc);	
		}
		
		public static void main(String[] args) {
			
			menu();
			
		}

	
}
