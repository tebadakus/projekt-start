package kalkulatorTEB;




/*
 * Zadanie:
 * - poprzednio tworzony kalkulator wzbogacic o obsluge wyjatkow/bledow
 * - dodac funkcje, ktore beda mialy za zadanie wczytywac wartosci do kalkulatora 
 * na wzor kodu ponizej (czyli wczytujaca jedynie wartosci typu float i zadne inne)
 * - przy dzieleniu takze zastapic potencjalny if na try - catch
 * - dodatkowo w przypadku wyjatku dzielenia przez zero sprawdzic funkcjonowanie klauzuli
 * finally; przyklad:
 * try {
 * 		//kod
 * }
 * catch (Exception e) {
 * 		//obsluga bledu, np. jakis komunikat
 * }
 * finally {
 * 	    //kod - wlasnie, jak on sie wykona?
 * }
 * 
 * ZADANIE 2:
 *v  - przetowrzyc zmienne na float
 *v  - doprowadzic do momentu by menu dzialalo do chwili az uzytkownik nie wpisze liczby zaknaczajacej
 * 		dzialanie programu (mozna wykorzystac rekurencje)
 *v  - stworzyc potegowanie za pomoca rekurencji
 * - zastanowic sie (i najlepiej wykonac) w jaki sposob wysietlic uzytkownik wszystkie podane
 * przez niego liczby, z ktorych wyswietlamy mu wynik (np. wyswietlic mu 10 liczb ktore wprowadzil)
 * - obsluzyc wszelkie bledy wprowadzania (np. podales zero albo podales wartosc ujemna - szczegolnie
 * tyczy sie silni)
 * - zobaczyc czym jest i wprowadzic modulo (i sprawdzic czy/czemu nie dziala na zmiennoprzecinkowych).
 */

import java.util.NoSuchElementException;
import java.util.Scanner;

public class KalkulatorPrzesileniaBledy {
	
	static float wczytajInt() {
		@SuppressWarnings("resource")
		var scan = new Scanner(System.in);
		try {
			//return Integer.parseInt(scan.next());
			return Float.parseFloat(scan.next());
			
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int!");
		}
		return 0;
	}
	
	static int wczytajUInt() {
		@SuppressWarnings("resource")
		var scan = new Scanner(System.in);
		//String w = scan.next();
		int ret = -1;
		try {
			//ret = Float.parseFloat(scan.next());
			ret = Integer.parseInt(scan.next());
			//to informacja, nie blad!
			if (ret<0) println("Podana wartosc jest mniejsza od zera!");
			return ret;
		}
		catch (Exception e) {
			System.err.println("Wpisałes wartosc inna niz int!");
		}
		return -1;
	}
	
	//*******uproszczony*print********
	static void print(Object o) {
		System.out.print(o);
	}
	//*******uproszczony*println******
	static void println(Object o) {
		System.out.println(o);
	}
	//********************************
	
	static int wczytajIlosc() {
		int ilosc=0;
		print("Podaj na ilu wartosciach chcesz dokonac wybranej operacji: ");
		return ilosc= wczytajUInt();
	}
	//********************************
	static int wczytajStopien() {
		int ilosc=0;
		print("Podaj jakiego stopnia ma być operacja: ");
		return ilosc= wczytajUInt();
	}
	//**********wyswietl*liczby*podane*************************************
	static float pokazWpisaneLiczby(int i) {
	print("podaj liczbe "+(i+1)+": ");
	float wczytana;
	return  wczytana=wczytajInt();
	}
	//*********************************************************************
	static void menu() {
		println("---------------------------------------");
		println("Kalkulator w Java");
		println("Wybierz jedna z operacji:");
		println("1. Dodawanie\n2. Odejmowanie"
				+ "\n3. Mnożenie\n4. Dzielenie"
				+ "\n5. Potegowanie\n6. Pierwiastkowanie"
				+ "\n7. Silnia\n8. Wyjscie");
		System.out.print("Wybor: ");

		int wybor;
		do {
			wybor=wczytajUInt();
		} while(wybor<0);
		
	    switch(wybor) {
		    case 1: dodawanie(wczytajIlosc()); break;
		    case 2: odejmowanie(wczytajIlosc()); break;
		    case 3: mnozenie(wczytajIlosc()); break;
		    case 4: dzielenie(wczytajIlosc()); break;
		    case 5: println("Wynik potęgowania: "+potegowanie(wczytajStopien())); break;
		    case 6: pierwiastkowanie(wczytajStopien()); break;
		    case 7: println("Wynik silni: "+silnia(wczytajStopien())); break;
		    case 8: return;
	    }
	    @SuppressWarnings("resource")
		Scanner scan=new Scanner(System.in);
	    println("---------------------------------------");
	    print("Aby kontynuować wciśnij dowolny klawisz !");
	    	scan.nextLine();
	    menu();
	}
	
	static void dodawanie(int ilosc) {
		float suma=0,wczytana;
		//ilosc++ -> ilosc=ilosc+1;
		for (int i=0;i<ilosc;i++) {
			/*
			 * Ponizszy kod ma tzw. zlozonosc kwadratowa. Kod tego typu to przewaznie 
			 * wykonywanie zagniezdzonych petli (petla glowna, podpetla/podtle). Tego typu
			 * rozwiazania sa "najciezsze" dla komputera i wykonuja sie przewaznie
			 * dwukrotnie wolniej niz kod tzw. liniowy (czyli posiadajacy jedna petle)
			 * Ponizsza zagniezdzona petla nie czyni kodu stricte kawadratowym bo jej zadaniem
			 * jest wymusic na uzytkowniku wpisania wartosci roznej od zera; jezeli od razu
			 * taka poda to funkcja zachowa sie liniowo
			 */
			do {
			//	print("podaj liczbe "+(i+1)+": ");
			//	wczytana=wczytajInt(); 
				wczytana=pokazWpisaneLiczby( i);
				
			}while(wczytana==0);
			suma+=wczytana;
		}
		println("Suma podanych liczb to: " + suma);
	}
	
	static void odejmowanie(int ilosc) {
		
		float odejm=0,wczytana;
		//ilosc++ -> ilosc=ilosc+1;
		for (int i=0;i<ilosc;i++) {
			/*
			 * Ponizszy kod ma tzw. zlozonosc kwadratowa. Kod tego typu to przewaznie 
			 * wykonywanie zagniezdzonych petli (petla glowna, podpetla/podtle). Tego typu
			 * rozwiazania sa "najciezsze" dla komputera i wykonuja sie przewaznie
			 * dwukrotnie wolniej niz kod tzw. liniowy (czyli posiadajacy jedna petle)
			 * Ponizsza zagniezdzona petla nie czyni kodu stricte kawadratowym bo jej zadaniem
			 * jest wymusic na uzytkowniku wpisania wartosci roznej od zera; jezeli od razu
			 * taka poda to funkcja zachowa sie liniowo
			 */
			do {
			//	print("podaj liczbe "+(i+1)+": ");
			//	wczytana=wczytajInt(); 
				wczytana=pokazWpisaneLiczby( i);
				
			}while(wczytana==0);
			odejm-=wczytana;
		}
		println("Odejmowanie podanych liczb to: " + odejm);
	}
		
	
	
	static void mnozenie(int ilosc) {
		float wynik=1;
		//1. Sprawdz czy ilosc jest wieksze od zera; jezeli jest wykonac kod while i zmniejsz 
		//ilosc o 1
		while(ilosc-->0) {
			//2. zmienna tymczasowa; twrzona KAZDORAZOWO przy kolejnym dzialaniu petli (np. 500 wywolan
			//to 500 razy utworzona zmienna tymczasowa -> nie jest to do konca efektywne
			float tymczasowa;
			//3. w kodzie Java czesto mozna sptkac tego typu rozwiazanie; w instrukcji if
			//MOZEMY przypisywac wartosci do zmiennych by nastepnie te wartosci (juz przypisanych
			//do zmiennej) porwnywac (poddawac operacjom porownan logicznych); oszczedza to
			//linjke kodu i dla niektorych jest czytelniejsze (nie dla wszystkich)
			if((tymczasowa=wczytajInt())==0)
			//4. jezeli zmienna o nazwie tymczasowa zawiera wartosc 0 - podnosimy ilosc 
			// (sterowanie iloscia wykonania petli) o 1, co znaczy, ze uzytkownik musi jeszcze raz
			//powtorzyc wprowadzanie liczby
				ilosc++;
			else
			//5. uzytkownik wprowadzil liczbe inna od 0; program wykonuje sie dalej bez bledow
				wynik*=tymczasowa;
		}
		//powyzsze rozwiazanie jest czysto liniowe; zawiera tylko jedna petle, ktora wykonuje sie
		//liniowo (wieksza wartosc ilosc -> dluzej, mniejsza wartosc -> krocej).
		println("Wynik mnozenia podanych liczb to: " + wynik);
	}
	
	static void dzielenie(int ilosc) {
		
	}
	
	
	
	//**************************************************
	static float potegowanie(float stopien) {
		//print(stopien);
		print("podaj liczbe: ");
		float liczba=wczytajInt();
		float mnoznik=liczba;
		return potegowanie(stopien,liczba,mnoznik);
		
	}
	
	static float potegowanie(float stopien, float liczba, float mnoznik) {
		//print(stopien);
		//final float wynik=liczba;
		if (stopien==0) return 1;
		if (stopien==1) return liczba;
		
		return potegowanie(stopien-1, liczba*mnoznik,mnoznik);
	}
	//**************************************************
	
	
	static void pierwiastkowanie(int ilosc) {
		
	}
	
	//funkcja przeciazajaca; to te funkcje wywoluje uzytkownik kodu (wpisuje z jakiej wartosci
	//bedzie liczona silnia); drugi parametr dolozony zostal w postaci literalu i wynosi 1;
	//dzieki temu funkcja poprawnie liczy wartosc silni (jezeli uzytkownik poda wartosc 0 - wynosi 1;
	//mnozenie 1*1 tez daje jeden; kazde pozostale da prawidlowy wynik)
	static int silnia(int ilosc) {
		return silnia(ilosc, 1);
	}
	
	//rekurencja czyli wywolywanie funkcji przez sama siebie; w ponizszym wypadku nasza silnia
	//musi zapamietywac swoj wynik - stworzylismy zmienna pomocnicza wynik; uzytkownik docelowy
	//funkcji nie powinien nim jednak zaprzatac sobie glowy (bez sensu by przy kazdym wywolaniu
	//funkcji wpisywal w drugi paramatr wartosc 1) Stad wykorzystane zostalo takze przeciazenie
	//funkcji (funkcja napisana powyzej)
	static int silnia(int ilosc, int wynik) {
		if (ilosc==0) return wynik;
		return silnia(ilosc-1,wynik*ilosc);	
	}
	
	public static void main(String[] args) {
		
		menu();
		
	}
	
	
	/*
	 * Ponizej opis dzialania inkrementacji jezykowej
	 * int zmienna=1;
		//zmienna++ tzw. inkrementacja czyli zwiekszanie wartosci liczbowej o 1
		//wystepuja dwa rodzaje:
		//- postinkremetacja; nastepuje PO wykonaniu biezacej operacji. Ponizej biezaca operacja
		//jest wyswietlenie wartosci zmiennej zmienna; wyswietli sie nam wartosc 1 (nie 2)
		println("ZMIENNA " + (zmienna++));
		//ale ponizej zmienna bedzie juz miala wartosc 2
		println("ZMIENNA " + zmienna);
		//- preinkrementacja; nastepuje PRZED wykonaniuem biezacej operacji. Ponizej biezaca
		//operacja jest wyswietlenie wartosci zmiennej zmienna; wyswietli sie nam wartosc 3 (nie 2)
		println("ZMIENNA " + (++zmienna));
		//ponizej nadal bedzie to wartosc 3 (nic sie juz z ta zmienna nie dzieje)
		println("ZMIENNA " + zmienna);
		
	  * ISTNIEJE TAKZE DEKREMENTACJA (obnizanie wartosci o 1) i dziala analogicznie
	 */
}
