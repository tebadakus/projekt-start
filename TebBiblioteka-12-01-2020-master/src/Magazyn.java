import java.util.ArrayList;
import java.util.List;

import pozycje.Ksiazka;

public class Magazyn {
	List<Ksiazka> ksiazki;

	
	public void dodajKsiazka() {
		if (ksiazki == null) 
			ksiazki = new ArrayList<>();		
		Ksiazka k = new Ksiazka();
		k.dodajAutora("Jan Nowakowski");
		k.dodajAutora("Jan Kochanowski");
		k.dodajAutora("Maria Dąbrowska");
		k.dodajTytul("Opowieści niezwykłe");
		k.dodajRok(1800);
		k.dodajStrony(234);
		ksiazki.add(k);
		///////////////////////////////////////////
		k = new Ksiazka();
		k.dodajAutora("Julia Julianowska");
		k.dodajAutora("Magdalena Listowska");
		k.dodajAutora("Zenon Znak");
		k.dodajTytul("1001 opowiastek");
		k.dodajRok(1990);
		k.dodajStrony(145);
		ksiazki.add(k);
	}
	public void usunKsiazke(Object o) {
		System.out.println("Usuwam książkę "+o);
		int ind=0;
		for (Ksiazka ksiazka : ksiazki) {
			System.out.println("get ind"+ksiazki.get(ind));
			System.out.println("szukaj(o)"+ksiazka.szukaj(o));
			if(ksiazki.get(ind).equals(ksiazka.szukaj(o))) {
				ksiazki.remove(ind); break;
					
			}
			ind++;
		}
	}
	
	
	public void pokazKsiazki() {
		
		for (Ksiazka ksiazka : ksiazki) {
			System.out.println("---------------------------------------");
			System.out.println(ksiazka.pokazKsiazke());			
			System.out.println("---------------------------------------");
		}
	}
	
	
	
	public String szukaj(String s) {
		return szukaj(s, 0);
	}
	
	public String szukaj(String s, int tryb) {
		String out="";
		for (Ksiazka ksiazka : ksiazki) {
			String tmp = "";
			if (tryb == 0)
				tmp=szukajZawartosc(ksiazka.pokazKsiazke(), s);
			if (tryb == 1)
				tmp=szukajNaglowki(ksiazka, s);
			if (!tmp.isEmpty()) 
				out+="-------------------------------------\n"+ tmp
				+"-------------------------------------\n";
		}
		return out;
	}
	
	private String szukajZawartosc(String k, String s) {
		if (k.contains(s)) 
			return k;
		return "";
	}
	
	private String szukajNaglowki(Ksiazka k, String s) {
		if(k.szukaj(s) != null)
			return k.pokazKsiazke();
		return "";
	}
	
//	public String szukajSzybko(String s) {
//		String out="";
//		for (Ksiazka ksiazka : ksiazki) {
//			if (ksiazka.szukaj(s) != null) 
//				out+="-------------------------------------\n"+ ksiazka.pokazKsiazke()
//				+"-------------------------------------\n";
//		}
//		return out;
//	}
}
