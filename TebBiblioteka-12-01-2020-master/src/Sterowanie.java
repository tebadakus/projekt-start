import java.util.Scanner;


public class Sterowanie {
	Magazyn m;
	
	public Sterowanie(Magazyn m) {
		this.m=m;
	}
	
	void print(Object o) {
		System.out.println(o);
	}
	
	@SuppressWarnings("resource")
	void menu() {
		String czytaj = "5";
		int wybor = 5;
		
		print("Witaj w programie biblioteka.");
		print("Wybierz jedna z opcji:");
		print("1. Dodaj pozycje\n2. Pokaż pozycje\n3. Szukaj pozycje\n4. Usun pozycje\n5. Zakoncz");
		print("Twoj wybor: ");
		czytaj = new Scanner(System.in).next();
		try {
			wybor = Integer.parseInt(czytaj);
		}
		catch (Exception e) {
			wybor = 5;
		}
		switch (wybor) {
		case 1: m.dodajKsiazka(); break;
		case 2: m.pokazKsiazki(); break;
		case 3: print(m.szukaj("Julia Julianowska",1)); break;
		case 4: m.usunKsiazke("Julia Julianowska");break;
		default: print("Koncze program!");System.exit(0);
		}
		
	}

}
