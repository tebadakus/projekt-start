package pozycje;

import java.util.ArrayList;
import java.util.List;

public class Ksiazka {
	
	String tytul;
	List<String> autorzy;
	int rok_wydania;
	String opis;
	int strony;
	String isbn;
	String wydawnictwo;
	
	public Object szukaj(Object o) {
		return szukaj(o,true);
	}
	
	public Object szukaj(Object o, boolean fast) {
		if (isbn == o)
			return this;
		if (tytul == o)
			return this;
		if (Integer.valueOf(rok_wydania) == o)
			return this;
		if  (Integer.valueOf(strony) == o)
			return this;
		for (String autor : autorzy) {
			if (autor == o)
				return this;
		}
		if (wydawnictwo == o)
			return this;
		if (!fast)
			if (opis.contains(String.valueOf(o)))
				return this;
		return null;
	}
	
	public Object znajdzRok(int p) {
		return znajdzRok(p, p);
	}
	
	public Object znajdzRok(int p, int k) {
		if (rok_wydania >= p && rok_wydania <= k)
			return this;
		return null;
	}
	
	public void dodajTytul(String s) {
		tytul = s;
	}
	
	public void dodajAutora(String s) {
		if (autorzy == null) {
			autorzy = new ArrayList<>();
		}
		autorzy.add(s);
	}
	
	public void dodajRok(int i) {
		rok_wydania=i;
	}
	
	public void dodajOpis(String s) {
		opis=s;
	}
	
	public void dodajStrony(int i) {
		strony=i;
	}
	
	public void dodajISBN(String s) {
		isbn=s;
	}
	
	public void dodajWydawnictwo(String s) {
		wydawnictwo=s;
	}
	
	public String pokazKsiazke() {
		String s = "";
		s+="Tytuł książki: \"" + tytul + "\"\nAutorzy: ";
		for (String imieNazwisko : autorzy) {
			s+=imieNazwisko+", ";
		}
		s=s.substring(0, s.length()-2);
		s+="\nRok wydania: " + rok_wydania + "\n";
		s+="Ilosc stron: " + strony + "\n";
		s+="Wydawnictwo: " + wydawnictwo + "\n";
		return s;
	}
	
	
}

