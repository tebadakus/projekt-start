import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import narzedzia.Wspolne;
import pozycje.Ksiazka;

/*
 * Poni�sza klasa, w przeciwie�stwie do Sterowanie, nie zosta�a
 * poszerzona o bazow� klas� Wspolne. Dlatego te�
 * ka�d� metod� oraz funkcj� musimy wywo�ywa� ze wskazaniem na
 * klas� Wspolne
 */

public class Magazyn {
	List<Ksiazka> ksiazki;
	//typ wyliczeniowy, pozwalaj�cy na 
	//zamienianie mniej oczywistych danych na bardziej przyjazne
	//dla nas
	public static enum TRYB {
		//nazwy i przypisane do nich warto�ci licznowe
		//nalezy pami�ta�, �e w tej postaci s� one KONSTRUKORAMI TYPU
		OGOLNY(0),
		SZCZEGOLOWY(1),
		LATA(2);
		
		//zmienna przechowywana w typie enum odpowiadaj�ca za 
		//warto�� liczbow� przypisan� do nazwy enum
		private final int num;
		
		//konstruktor typu; konstruktor powinien (musi)by� 
		//prywatny by nikt poza samym typem enum nie m�g� go wywowa�
		//wywo�ywany jest niejawnie (czyli po�rednio podczas dzia�ania
		//kodu) gdy programista wywo�uje jedn� z nazw zapisan� w enum
		//(w naszym wypadku OGOLNY, SZCZEGOLOWY, LATA)
		private TRYB(int num) {this.num=num;}
		
		public int get() {return this.num;}

	}
	
	public void dodajKsiazka() {
		if (ksiazki == null) 
			ksiazki = new ArrayList<>();		
		Ksiazka k = new Ksiazka();
		String tmp = null;
		//TODO
		
		while(tmp==null) {
			//zastanowi� si�, czy poni�sze wyra�enie nie powinno dzia�a� w Ksiazka.dodajAutora();
			//pop prostu zamast parametru czy nie powinno by� odpytywania w samej metodzie
			k.dodajAutora(Wspolne.getData("Podaj autora (imie nazwisko): "));
			tmp=Wspolne.getData("Czy doda� kolejnego autora(Y/N)? ");
			if (tmp.charAt(0)=='y'||tmp.charAt(0)=='Y') 
				tmp=null;
		}
		tmp=null;
		k.dodajTytul(Wspolne.getData("Podaj tytu� (Ksi��ki): "));

		
		//dwa while powinny by� zamienione na jedn� funkcj�
		//by nie powiela� kodu 
		while (tmp==null) {
			try {
				k.dodajRok(Integer.valueOf(Wspolne.getData("Podaj rok wydania (np. 1900): ")));
				tmp="ok";
			}
			catch (Exception e) {
				System.out.println("PODALES ZLE DANE, JESZCZE RAZ!");
			}
		}
		tmp=null;
		while (tmp==null) {
			try {
				k.dodajStrony(Integer.valueOf(Wspolne.getData("Podaj ilo�� stron (np. 100): ")));
				tmp="ok";
			}
			catch (Exception e) {
				System.out.println("PODALES ZLE DANE, JESZCZE RAZ!");
			}
		}
		//warto by by�o doda� mo�liwo�� wpisania ISBN, wydawnictwa
		//oraz opisu ksi��ki; je�eli u�ytkownik nic nie poda (lub
		//poda puste ci�gi, np. ze spacjami) to mo�na zapyta� go czy si�
		//nie pomyli� i nie chce poda� danego elementu jeszcze raz
		
		ksiazki.add(k);
		
	}
	
	
	public void pokazKsiazki() {
		
		for (Ksiazka ksiazka : ksiazki) {
			System.out.println("---------------------------------------");
			System.out.println(ksiazka.pokazKsiazke());			
			System.out.println("---------------------------------------");
		}
	}
	
	public String szukaj(String s) {
		return szukaj(s, 0);
	}
	
	//alternatywna posta� funkcji pozwalaj�ca na podawanie danych
	//z typu enumerycznego (wyliczeniowego) - jego opis wy�ej
	public String szukaj(String s, String tryb) {
		return szukaj(s, TRYB.valueOf(tryb).get());
	}
	
	public String szukaj(String s, int tryb) {
		String out="";
		for (Ksiazka ksiazka : ksiazki) {
			String tmp = "";
			if (tryb == 0)
				tmp=szukajZawartosc(ksiazka.pokazKsiazke(), s);
			if (tryb == 1)
				tmp=szukajNaglowki(ksiazka, s);
			if (tryb == 2) szukajRok(ksiazka, Integer.valueOf(s.split(" ")[0]), Integer.valueOf(s.split(" ")[1]));
			if (!tmp.isEmpty()) 
				tmp=
				out+="-------------------------------------\n"+ tmp
				+"-------------------------------------\n";
		}
		if (out.isEmpty())
			out="Nie znaleziono �adnej pozycji wedle podanych kryteri�w!";
		return out;
	}
	
	private String szukajRok(Ksiazka k, int p, int ko) {
		if (k.znajdzRok(p,ko)!=null)
			return k.pokazKsiazke();
		return null;
	}
	
	private String szukajZawartosc(String k, String s) {
		if (k.contains(s)) 
			return k;
		return "";
	}
	
	private String szukajNaglowki(Ksiazka k, String s) {
		if(k.szukaj(s) != null)
			return k.pokazKsiazke();
		return "";
	}
	
//	@SuppressWarnings("resource")
//	private String getData(String sS) {
//		Scanner s = new Scanner(System.in);
//		String str=null;
//		while (str==null) {
//			print(sS, false);
//			str = s.nextLine();
//		}	
//		return str;
//	}
//	
//	void print(Object o) {
//		print(o, true);
//	}
//	
//	void print(Object o, boolean n) {
//		System.out.print(String.valueOf(o) + (n ? '\n' : ""));
//	}
	
//	public String szukajSzybko(String s) {
//		String out="";
//		for (Ksiazka ksiazka : ksiazki) {
//			if (ksiazka.szukaj(s) != null) 
//				out+="-------------------------------------\n"+ ksiazka.pokazKsiazke()
//				+"-------------------------------------\n";
//		}
//		return out;
//	}
}
