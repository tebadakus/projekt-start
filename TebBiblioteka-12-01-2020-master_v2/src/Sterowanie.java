import java.util.Scanner;

import narzedzia.Wspolne;

/*
 * Poni�sza klasa zosta�a poszerzona o klas� bazow� Wspolne. 
 * Dzi�kie temu zabiegowi wszystkie jej sk�adowe (metody oraz pola)
 * mozna u�ywa� w naszej klasie Sterowanie bez wywo�ywania nazwy
 * klasy Wspolne 
 */

public class Sterowanie extends Wspolne {
	Magazyn m;
	
	public Sterowanie(Magazyn m) {
		this.m=m;
	}
	
	@SuppressWarnings("resource")
	void menu() {
		String czytaj = "5";
		int wybor = 5;
		print("Witaj w programie biblioteka.");
		print("Wybierz jedna z opcji:");
		print("1. Dodaj pozycje\n2.Znajdz pozycje\n3. Modyfikuje pozycje\n4.Usun pozycje\n5.Wyswietl wszystko\n6.Zakoncz");
		print("Twoj wybor: ");
		czytaj = new Scanner(System.in).next();
		try {
			wybor = Integer.parseInt(czytaj);
		}
		catch (Exception e) {
			wybor = 5;
		}
		switch (wybor) {
		case 1: m.dodajKsiazka(); break;
		case 2: szukanie(); break;
		case 3: break;
		case 4: break;
		case 5: m.pokazKsiazki(); break;
		default: print("Koncze program!");System.exit(0);
		}
		
	}
	
	@SuppressWarnings("resource")
	private void szukanie() {
		print("Szukaj po nast�puj�cych opcjach: ");
		print("1.Ci�gu znakowym\n2.Latach wydania\n3.Liczbach stron\n4.Wydaniach\n5.Indeksie tytu��w\n6.Indeksie autor�w");
		print("Tw�j wyb�r: ", false);
		String w = new Scanner(System.in).next();
		try {
			switch(Integer.valueOf(w)) {
				case 1: znajdzPozycje(); break;
				case 2: znajdzRok(); break;
				default: print("Brak opcji w menu b�dz opcja niedost�pna!");
			}
		}
		catch (Exception e) {
			Wspolne.print("Poda�e� co� innego ni� liczba!");
		}
	}
	
	@SuppressWarnings("resource")
	private void znajdzPozycje() {
		Scanner s = new Scanner(System.in);
		String str = getData("Podaj szukan� fraz�: ");
		String p = null;
		while (p==null) {
			p= Wspolne.getData("Czy szukanie ma by� dok�adne (Y/N)? ");
			if ((p.charAt(0) != 'Y' || p.charAt(0) !='y') && (p.charAt(0) != 'N' || p.charAt(0) != 'n')) 
				p=null;
		}
		Wspolne.print(m.szukaj(str,(p.charAt(0)=='Y'||p.charAt(0)=='y') ? 1 : 0));
		
	}
	
	
	
	private void znajdzRok() {
		
		int p,k;
		p=k=0;
		while(p==0) {
			try {
				p=Integer.valueOf(Wspolne.getData("Podaj rok pocz�tku przeszukiwania: "));
			}
			catch (Exception e) {
				
			}
		}
		while(p==0) {
			try {
				k=Integer.valueOf(Wspolne.getData("Podaj rok ko�ca przeszukiwania: "));
			}
			catch (Exception e) {
				String a = null;
				while(a==null) {
					a=getData("B��d. Zrezygnowa� z daty granicznej (T/N)?: ");
					if ((a.charAt(0) != 'Y' || a.charAt(0) !='y') && (a.charAt(0) != 'N' || a.charAt(0) != 'n')) 
						a=null;
				}
				
			}
		}
		//m.szukaj(String.valueOf(p) + " " + String.valueOf(k), Magazyn.TRYB.valueOf(Magazyn.TRYB.LATA.toString()).get());
	}
}
