package indeksy;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Autor {
	String imie,nazwisko;
	Date data_urodzin,data_smierci;
	String biografia;
	
	public Autor() {
		imie=nazwisko=biografia="";
		try {
			data_urodzin=data_smierci=new SimpleDateFormat("yyyy-MM-dd").parse("1970-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	//TODO
	//tutaj powinny by� kolejne konstruktory, kt�re pozwalaj� na dodanie
	//informacji o autorze bezpo�rednio przy tworzeniu obiekt�w
	//konstruktory powinny by� tak zrobione by jak najbardziej u�atwi� dodawanie podstawowych informacji
	
	public void setImie(String imie) {
		this.imie=imie;
	}
	
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	
	public void setImieNazwisko(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	
	public boolean setUrodziny(String data) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			data_urodzin = df.parse(data);
			return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void setUrodziny(Date date) {
		data_urodzin=date;
	}
	//TODO
	//mo�na doda� mo�liwo�� dodawania urodzin jako roku, miesi�ca oraz dnia niezale�nie
	//b�dz to przez int, b�dz to jako niezale�ne ci�gi znakowe roku, miesi�ca oraz dnia
	
	public boolean setSmierc(String data) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			data_smierci = df.parse(data);
			return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void setSmierc(Date date) {
		data_smierci=date;
	}
	//TODO
	//mo�na doda� mo�liwo�� dodawania �mierci jako roku, miesi�ca oraz dnia niezale�nie
	//b�dz to przez int, b�dz to jako niezale�ne ci�gi znakowe roku, miesi�ca oraz dnia
	
	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}
	/*************************************************************************/
	
	public String getImie() {
		return this.imie;
	}
	
	public String getNazwisko() {
		return this.nazwisko;
	}
	
	public String getImieNazwisko() {
		return this.imie + " "+ this.nazwisko;
	}
	
	public String getUrodziny() {
		//SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		return new SimpleDateFormat("yyyy/MM/dd").format(data_urodzin);
	}
	
	public String getSmierc() {
		if (!data_smierci.equals(data_urodzin))
			return new SimpleDateFormat("yyyy/MM/dd").format(data_smierci);
		return "";
	}
	//TODO
	//daty urodzin oraz �mierci powinny mie� mo�liwo�� wy�wietlania tak�e jako
	//ich natywnego typu (Date) b�dz np. samego roku (bez pozosta�ych element�w)
	//mo�na te� pomy�le� o zwracaniu miesi�ca dnia i roku jako liczbach (do por�wna� z innymi datami)
	
	//ciekawym rozwi�zaniem by�oby napisa� metod� pobieraj�c� wiek autora (liczony z 
	// daty urodzin i aktualnej daty lub z daty urodzin i �mierci - por�wnywanie dat!)
	
	public String getBiografia() {
		return biografia;
	}
	
	//jest to przyk�ad nadpisania metody maj�cej ju� swoje dzia�anie i
	//zastosowanie zdefiniowane w obiekcie nadrz�dnym (w tym wypadku w samym
	//Object). Pozwala to na zmienienie domy�lnego zachowania
	//tej�e funkcji
	@Override
	public String toString() {
		return getImieNazwisko();
	} 
	
	@Override
	public boolean equals(Object obj) {
		//TODO tutaj pasowa�oby sprawdza� czy aby przekazny obiekt to Autor
		
		Autor o = (Autor)obj;
		//TODO
		//tutaj nale�a�oby por�wnywa� obie warto�ci znak po znaku
		//z tego powodu, �e niekoniecznie ZAWIERA� znaczy by� r�wnym!
		//(przyk�ad ze znak�w Y/N w pozosta�ej cz�ci kodu
		//por�wnanie powinno dzia�a� tak�e po dacie urodzin/�mierci (dok�adniejsze dane)
		if (o.getImie().contains(imie) && o.getNazwisko().contains(nazwisko))
			return true;
		return false;
	}
	
	public boolean contains(Object s) {
		
		return (getImieNazwisko().contains(String.valueOf(s))) ? true : false;
	}
}
