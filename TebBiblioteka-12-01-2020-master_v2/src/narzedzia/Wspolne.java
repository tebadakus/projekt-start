package narzedzia;

import java.util.Scanner;

public class Wspolne {
	//wszystkie metody klasy Wspolne s� statyczne - dzi�kie temu
	//nie musimy tworzy� obiektu by m�c wykona� ich kod
	
	public static void print(Object o) {
		print(o, true);
	}
	
	public static void print(Object o, boolean n) {
		System.out.print(String.valueOf(o) + (n ? '\n' : ""));
	}
	
	@SuppressWarnings("resource")
	public static String getData(String sS) {
		Scanner s = new Scanner(System.in);
		String str=null;
		while (str==null) {
			print(sS, false);
			str = s.nextLine();
		}	
		return str;
	}
	
	
}

