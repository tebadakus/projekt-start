package pozycje;

import java.util.ArrayList;
import java.util.List;

import indeksy.Autor;

public class Ksiazka {
	
	String tytul;
	//dobrze by�oby gdyby autorzy byli wybierani z listy dost�pnych 
	//autor�w, kt�ra by�oby tworzona na podstawie klasy Autor z paczki
	//indeksy
	List<Autor> autorzy;
	int rok_wydania;
	String opis;
	int strony;
	String isbn;
	String wydawnictwo;
	
	public Object szukaj(Object o) {
		return szukaj(o,true);
	}
	
	public Object szukaj(Object o, boolean fast) {
		if (isbn == o)
			return this;
		if (tytul == o)
			return this;
		if (Integer.valueOf(rok_wydania) == o)
			return this;
		if  (Integer.valueOf(strony) == o)
			return this;
		for (Autor autor : autorzy) {
			//TODO 
			//to przeszukiwanie nale�y zmieni� tak by przezukiwa� np. po inicja�ach
			if (autor.contains(o))
				return this;
		}
		if (wydawnictwo == o)
			return this;
		if (!fast)
			if (opis.contains(String.valueOf(o)))
				return this;
		return null;
	}
	
	public Object znajdzRok(int p) {
		return znajdzRok(p, p);
	}
	
	public Object znajdzRok(int p, int k) {
		if (rok_wydania >= p && rok_wydania <= k)
			return this;
		return null;
	}
	
	public void dodajTytul(String s) {
		tytul = s;
	}
	
	public void dodajAutora(String s) {
		if (autorzy == null) {
			autorzy = new ArrayList<>();
		}
		//TODO
		//do zmiany - nale�y zastanowi� si� czy w tej funkcji nie powinno by�
		//zadawania pyta� o kolejne pola dla autora (imie, nazwisko, daty, biografia)
		//i ewentualnie od razu nie dodawa� tych danych do konstruktora
		Autor a = new Autor();
		//TODO
		//nale�y sprawdza� wielko�� liter wpisanych przez u�ytownika
		//je�eli b�d� r�nej wielko�ci nale�a�oby zmieni� pierwsz� liter�
		//na du��, pozosta�e na ma�e; zak�aamy kod ASCII!
		a.setImieNazwisko(s.split(" ")[0], s.split(" ")[1]);
		for (Autor autor : autorzy) {	
			if (autor.equals(a)) {
				return;
			}
		}
		autorzy.add(a);
	}
	
	public void dodajRok(int i) {
		rok_wydania=i;
	}
	
	public void dodajOpis(String s) {
		opis=s;
	}
	
	public void dodajStrony(int i) {
		strony=i;
	}
	
	public void dodajISBN(String s) {
		isbn=s;
	}
	
	public void dodajWydawnictwo(String s) {
		wydawnictwo=s;
	}
	
	public String pokazKsiazke() {
		String s = "";
		s+="Tytu� ksi��ki: \"" + tytul + "\"\nAutorzy: ";
		for (Autor autor : autorzy) {
			//dzi�ki nadpisaniu toString() nie musimy tutaj pisa�
			//np. autor.getImieNazwisko() - toString wykonuje si� niejawnie
			//w chwili pr�by konwersji to typu String
			s+=autor+", ";
		}
		s=s.substring(0, s.length()-2);
		s+="\nRok wydania: " + rok_wydania + "\n";
		s+="Ilosc stron: " + strony + "\n";
		s+="Wydawnictwo: " + wydawnictwo + "\n";
		return s;
	}
	
	
}

