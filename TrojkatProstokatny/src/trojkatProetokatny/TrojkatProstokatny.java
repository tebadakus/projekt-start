package trojkatProetokatny;
import java.util.Scanner;

public class TrojkatProstokatny {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
 *  Do sprawdzenia czy da się z trzech odcinków zbudować trojkąt prostokątny
 *  można wykorzystać równość wynikającą z twierdzenia Pitagorasa, 
 *  z której wyznaczamy długość przeciwprostokątnej:  a^2 +b^2 = c^2 gdzie a i b są przyprostokątnymi.
 *  Przykład boków trojkąta to : 3,4,5 lub 7,24,25
 */
	Scanner input = new Scanner(System.in);
	int wybor=0;
	while (wybor!=99) {
		System.out.println("---------------------------------------------------------");
		System.out.println("Wybierz jedną z operacji");
		System.out.println(" 1 . Sprawdz czy z trzech boków powstanie trójkat prostokątny\n "
				+ "2 . Oblicz powierzchnie trójkąta prostokątnego znając dwie przyprostokątne\n "
				+ "3 . Długość przeciwprostokątnej trójkąta\n 99. Koniec");
		wybor = input.nextInt();
	if (wybor==1) {
				
	System.out.println("---------------------------------------------------------");
	System.out.println("Proszę podać trzy długości boków trójkąta");
		System.out.println("Podaj bok1: ");
		double bok1 = input.nextDouble();
		System.out.println("Podaj bok2: ");
		double bok2 = input.nextDouble();
		System.out.println("Podaj bok3: ");
		double bok3 = input.nextDouble();
	if (bok3*bok3 == bok1*bok1 + bok2*bok2 ) {
		System.out.println("Trójkąt jest prostokątny gdzie boki "+bok1+" i "+bok2+" są przyprostokątnymi");
	} else if (bok1*bok1 == bok2*bok2 + bok3*bok3) {
		System.out.println("Trójkąt jest prostokątny gdzie boki "+bok2+" i "+bok3+" są przyprostokątnymi");
	} else if(bok2*bok2 == bok3*bok3 + bok1*bok1) {
		System.out.println("Trójkąt jest prostokątny gdzie boki "+bok3+" i "+bok1+" są przyprostokątnymi");
	} else {
		System.out.println("Z podanych boków "+bok1+" i "+bok2+" i "+bok3+" nie powstanie trójkąt prostokątny");
	}
	}
	if (wybor==2) {
		System.out.println("-----------------------------------------");
		System.out.println("Proszę podać dwie długości boków przyprostokątnych trójkąta");
			System.out.println("Podaj bok1: ");
			double bok1 = input.nextDouble();
			System.out.println("Podaj bok2: ");
			double bok2 = input.nextDouble();
		System.out.println("Powierzchnia trójkąta prostokątnego wynosi: "+(bok1*bok2/2));
	}
	if (wybor==3) {
		System.out.println("-----------------------------------------");
		System.out.println("Proszę podać dwie długości boków przyprostokątnych trójkąta");
			System.out.println("Podaj bok1: ");
			double bok1 = input.nextDouble();
			System.out.println("Podaj bok2: ");
			double bok2 = input.nextDouble();
			double c=bok1*bok1+bok2*bok2;
					
		System.out.println("Długość przeciwprostokątnej to: "+(Math.sqrt(c)));
	}
	
	if (wybor>3) {
		System.out.println("Koniec programu !");
	}
	}
}
}
