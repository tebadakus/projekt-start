/*
 * DO ZROBIENIA:
 * - wyeliminowac problem przetawienia szyku wielomianu (np. zamiast 2x^2+3x-2 -> 3x-2+2x^2)
 * - obsluzyc pozostale wyjatki i ewentualne bledy
 * - "nauczyc" program osblugi wielomianow wyzszego stopnia
 * 
 * PROGRAM + ALGORYTM
 * - napisac program wyliczajacy pochodne I i II stopnia z podanych wielomianow
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main().wykonajDzialanie();
	}

	
	String wzor=" ";
	double a[]; // parametry wielomianu a,b,c,....
	double d=0;
	double x1=0;
	double x2=0;
	
	
	@SuppressWarnings("resource")
	void wykonajDzialanie() {
		System.out.println();
		System.out.print("Podaj swoj wielomian 2go stopnia w formie\n"
				+ " ax^2 + bx + c :");
		wzor=new Scanner(System.in).nextLine();
		//sortujWielomian();// sortuje wielomian do postaci x^n+x^(n-1)+....
		//znajdzZmienne(); //do wlaczenia po uruchomieniu sortowania.!!!!!!!!!!!
		//  x^2+7x-9
		//  x^2+x-9
		// x^3+x^2-9
		// x^4+x^3-9x^2+2x-7
	
		
		
		// do usuniecia jesli zadziala!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		System.out.println(wzor.matches("[x]{2}"));
		if(wzor.indexOf("x^")!=-1 && wzor.indexOf("x")!=-1) {
			System.out.println("brakuje c - if x");
			
		//!!!!!!!!!!!!!!!!!! musi ustawic b=0 lub c=0
			
		}else {
			System.out.println(" brakuje b - else x");
		}
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
	
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
	void sortujWielomian() {
		wzor=wzor.replaceAll("[\\\\#!@%`\\*()_=~{}\\[\\]\\?/<>,.|]?", "");
		System.out.println(wzor);
		wzor=wzor.replaceAll("[a-zA-Z&&[^xX]]?", "");
		wzor=wzor.replace(" ","");	//usuwa spacje
		
		
		
		if (wzor.split("[\\^]{1}[^2]{1}").length!=1 || wzor.indexOf('^')==-1) {//bylo ==2 zamiast !=2
			//System.out.println(wzor.split("\\^2").length);
			//if (wzor.split("\\^2").length!=2) { 
				System.out.println(wzor.split("\\^[^2]{1}")[0]);
				System.out.println("Podales zly rodzaj wielomianu! Koncze program!");
				System.exit(-1);
			}
		
		
		
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		
		System.out.println(wzor.split("[x^]++&&[x]++")[0]);
		if(wzor.split("[\\^[x]]").length==2) {
			System.out.println((wzor.split("[x^]&&[x]").length)+"brakuje c - if x");
			
		//!!!!!!!!!!!!!!!!!! musi ustawic b=0 lub c=0
			
		}else {
			System.out.println((wzor.split("[x^]&&[x]").length)+" brakuje b - else x");
		}
		
		
		
		
		wzor=wzor.replace("+", " +"); // dodaje spacje przed znakami aby podzielic
		wzor=wzor.replace("-", " -"); // string z zachowaniem znakow w miejscu spacji.
		
		String tmp[] = wzor.split(" "); // podział po spacji
		tmp[0]=tmp[0].replaceFirst("", "+"); //dopisanie do pierwszego podanego czynnika wielomianu znaku
		
		int j=0;
		for (String val:tmp) {
			System.out.println(tmp[j]+" podział na elementy wielomianu");
		//	System.out.println(reverse(tmp[j])); // odwrocenie do sortowania po potedze!!!!
			j++;
		}
		System.out.println(j);
		/*
		    +2x^4 podział na elementy wielomianu
			+x^3 podział na elementy wielomianu
			-9^2 podział na elementy wielomianu
			+2x podział na elementy wielomianu
			-7 podział na elementy wielomianu
		 */
// !!!ZROBIC SORTOWANIE PO OSTATNIEJ CYFRZE ZEBY POSORTOWAC WIELOMIAN WRAZIE NIEPRAWIDLOWEGO 
		//PODANIA GO PRZEZ OPERATORA
		
		//sort Numbers from minimum ->max
		
		
		
				//***********************************************************************
	//			int n=0;
	//			while( n<tmp[j]) {
	//			for (int m=0; m<(j-1);m++) {
				
	//				if(numbers[m]>numbers[m+1]) {
	//					int temp = numbers[m];
	//					numbers[m]=numbers[m+1];
	//					numbers[m+1]=temp;
	//				} 
					
				//	System.out.print(numery[m]+", "); // sorting preview
					
	//			}	
				//	System.out.println(numery[amountNo-1]); 	//sorting preview
	//				n++;
	//			}
		// ******end of sorting cod*******************************************************
		
	}
	
	
	
	
	
	//****************************************************************************************
	// funkcja do odwracania stringow zeby sortowac po ostatniej cyfrze !!!Do Sprawdzenia !!!!
	String reverse(String str)
	{
	   if (str.length() == 1) return str;
	   else return reverse(str.substring(1)) + str.charAt(0);
	}
	//****************************************************************************************
	
	
	
	
	
	void znajdzZmienne( ) {
		wzor=wzor.replaceAll("[\\\\#!@%`\\*()_=~{}\\[\\]\\?/<>,.|]?", "");
		System.out.println(wzor);
		wzor=wzor.replaceAll("[a-zA-Z&&[^xX]]?", "");
		wzor=wzor.replace(" ","");	//usuwa spacje
		//wzor=wzor.strip(); //powinna usuwac spacje ale nie dziala
		//wzor=wzor.trim();
		System.out.println(wzor);
		
		System.out.println(wzor.split("[\\^]{1}[^2]{1}").length+" split wielomianu");
		
		if (wzor.split("[\\^]{1}[^2]{1}").length!=1 || wzor.indexOf('^')==-1) {//bylo ==2 zamiast !=2
		//System.out.println(wzor.split("\\^2").length);
		//if (wzor.split("\\^2").length!=2) { 
			System.out.println(wzor.split("\\^[^2]{1}")[0]);
			System.out.println("Podales zly rodzaj wielomianu! Koncze program!");
			System.exit(-1);
		}
		
		String tmp[] = wzor.split("\\*?x?X?[xX]{1}\\^?2?"); //wyodrebnienie wspolczynnikow wielomianu
		
		int rozmiar=0;
		for (String val:tmp) {
			System.out.println("tablica "+rozmiar+": "+tmp[rozmiar]);
			rozmiar++; // ustalenie rozmiaru tablicy dla przechowania a,b,c,....
		}
		
		a=new double[rozmiar];
		int i=0;
		for (String val:tmp) {
			if(tmp[i].isEmpty()||tmp[i].equals("+")||tmp[i].equals("-")) { //dodaje wartosc 1 gdy string pusty
				if (tmp[i].equals("-")) {	
					tmp[i]="-1";
					 a[i]=Double.valueOf(tmp[i]);
				}else{
					tmp[i]="1";
					a[i]=Double.valueOf(tmp[i]);
				}	
			}		
		System.out.println(Double.valueOf(tmp[i]));
		a[i]=Double.valueOf(tmp[i]);
		i++;
		}

	// ponizej sa bledy z obliczaniem dla x^2-9, oraz x^2-9x	 czyli niepelnych wielomianow
		
		if  (a[0]==0) {
			System.out.println("Wartosc a nie moze byc zerowa! Koncze program!");
			System.exit(-1);
		}
		d = a[1]*a[1] - 4*a[0]*a[2];
		x1 = (-a[1]-d)/2*a[0];
		System.out.println("Pierwsze miejsce zerowe to: " + x1);
		if(d!=0) {
			x2 = (-a[1]+d)/2*a[0];
			System.out.println("Drugie miejsce zerowe to: " + x2);
			
		}
		
	}
	
}
