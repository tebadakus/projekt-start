/*
 * DO ZROBIENIA:
 * - wyeliminowac problem przetawienia szyku wielomianu (np. zamiast 2x^2+3x-2 -> 3x-2+2x^2)
 * - obsluzyc pozostale wyjatki i ewentualne bledy
 * - "nauczyc" program osblugi wielomianow wyzszego stopnia
 * 
 * PROGRAM + ALGORYTM
 * - napisac program wyliczajacy pochodne I i II stopnia z podanych wielomianow
 * 
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main().wykonajDzialanie();
	}

	
	String wzor=" ";
	double a[]; // parametry wielomianu a,b,c,....
	double d=0;
	double x1=0;
	double x2=0;
	String wzorPochodna="";
	
	@SuppressWarnings("resource")
	void wykonajDzialanie() {
		System.out.println();
		System.out.print("Podaj swoj wielomian 2go stopnia w formie\n"
				+ " ax^2 + bx + c :");
		wzor=new Scanner(System.in).nextLine();
		sortujWielomian();// sortuje wielomian do postaci x^n+x^(n-1)+....
		znajdzZmienne(); // wyszykuje parametry wielomianu a,b,c
		pochodna(); // oblicza pochodna 1-go stopnia 
		
		//  x^2+7x-9
		//  x^2+x-9
		// x^3+x^2-9
		// x^4+x^3-9x^2+2x-7
		// x^2+x 
				// x^2+7 
				// x^2+x+7 
				// x^2 
				// 2x^2+2x+2
				// 22x^2+22x+22
		
	
	}
	//---------------------------------------------------------------------------------------------
	void sortujWielomian() {
		wzor=wzor.replaceAll("[\\\\#!@%`\\*()_=~{}\\[\\]\\?/<>,.|]?", "");
		System.out.println(wzor);
		wzor=wzor.replaceAll("[a-zA-Z&&[^xX]]?", "");
		wzor=wzor.replace(" ","");	//usuwa spacje
		
		
		
		if (wzor.split("[\\^]{1}[^2]{1}").length!=1 || wzor.indexOf('^')==-1) {//bylo ==2 zamiast !=2
			//System.out.println(wzor.split("\\^2").length);
			//if (wzor.split("\\^2").length!=2) { 
				System.out.println(wzor.split("\\^[^2]{1}")[0]);
				System.out.println("Podales zly rodzaj wielomianu! Koncze program!");
				System.exit(-1);
			}
		
		
		
		//**** sprawdza wielomian i gdy nie jest pełny dodaje 0 zeby***** 
		//**** po rozbiciu na wyrazy tablica miala 3 elementy************
		int howManyX=0;
		int isTherePlusMinus=0;
		
		for (int ind=0;ind <wzor.length();ind++) {
		if(wzor.substring(ind, ind+1).equals("x")) 
			howManyX++;
		if(wzor.substring(ind, ind+1).equals("+")||wzor.substring(ind, ind+1).equals("-"))
				isTherePlusMinus++;	
		}
		if (howManyX==2 && isTherePlusMinus==1) {
			wzor=wzor+"+0";
		}else if (howManyX==1 && isTherePlusMinus==0) {
			wzor=wzor+"+0x"+"+0";
		}else if (howManyX==1 && isTherePlusMinus==1){
			
			wzor=wzor+"+0x";
		}
		
		//*************************************************************
		
		
		
		System.out.println(wzor);
		wzor=wzor.replace("+", " +"); // dodaje spacje przed znakami aby podzielic
		wzor=wzor.replace("-", " -"); // string z zachowaniem znakow w miejscu spacji.
		String tmp[] = wzor.split(" "); // podział po spacji
		tmp[0]=tmp[0].replaceFirst("", "+"); //dopisanie do pierwszego podanego czynnika wielomianu znaku
		
		
		//*********************************************************************************
		//sortowanie wyrazow wielomianu - ustawienie w kolejnosci
		String temporary="";
		int j=0;
		for (String val:tmp) {
			if((tmp[j].substring(tmp[j].length()-1, tmp[j].length())).equals("2")) {
				if (j!=0) {
					if(tmp[j].matches("x")) {
					temporary=tmp[0];
					tmp[0]=tmp[j];
					tmp[j]=temporary;
					}
					// nie rob nic bo to ostatni element wielomianu bez x
				}
			}else if((tmp[j].substring(tmp[j].length()-1, tmp[j].length())).equals("x")) {
				if(j!=1) {
					temporary=tmp[1];
					tmp[1]=tmp[j];
					tmp[j]=temporary;
				}
			}
			j++;
		}
		wzor=tmp[0]+tmp[1]+tmp[2];
		System.out.println(tmp[0]+tmp[1]+tmp[2]+" sortowanie");
		//System.out.println(j); //wielkosc tablicy
		//**********************************************************************************
			
	
	}
	
	
	
	
	
	//--------------------------niepotrzebne---------------------------------------------------
	// funkcja do odwracania stringow zeby sortowac po ostatniej cyfrze !!!Do Sprawdzenia !!!!
	String reverse(String str)
	{
	   if (str.length() == 1) return str;
	   else return reverse(str.substring(1)) + str.charAt(0);
	}
	//-----------------------------------------------------------------------------------------
	
	
	
	
	//-----------------------------------------------------------------------------------------
	void znajdzZmienne( ) {
		
	//  2x^2+2x+2
		System.out.println(wzor+" znajdz wspolczynniki a,b,c");
		wzor=wzor.replaceFirst("\\+","");
		
		System.out.println(wzor);
		wzor=wzor.replace("+", " +"); // dodaje spacje przed znakami aby podzielic
		wzor=wzor.replace("-", " -"); // string z zachowaniem znakow w miejscu spacji.
		String tmp[] = wzor.split(" "); // podział po spacji
		tmp[0]=tmp[0].replaceFirst("", "+"); //dopisanie do pierwszego podanego czynnika wielomianu znaku
		
		//("\\*?x?X?[xX]{1}\\^?2?")
		int rozmiar=0;
		for (String val:tmp) {
			String temp[]=tmp[rozmiar].split("\\*?x?X?[xX]{1}\\^?2?");//!!!!!!!!!!!!????????------------------
			tmp[rozmiar]=temp[0];
			System.out.println(val+" tablicaZmienne "+rozmiar+": "+tmp[rozmiar]);
			
			rozmiar++; // ustalenie rozmiaru tablicy dla przechowania a,b,c,....
		}
		
		
		a=new double[rozmiar];
		int i=0;
		for (String val:tmp) {
			val.replace(" ","");
			if(val.isEmpty()||val.equals("+")||val.equals("-")) { //dodaje wartosc 1 gdy string pusty
				if (tmp[i].equals("-")) {	
					tmp[i]="-1";
					 a[i]=Double.valueOf(tmp[i]);
				}else{
					tmp[i]="1";
					a[i]=Double.valueOf(tmp[i]);
				}	
			}else {		
				a[i]=Double.valueOf(tmp[i]);
			}
			System.out.println(Double.valueOf(tmp[i]));
		i++;
		}

		
		if  (a[0]==0) {
			System.out.println("Wartosc ta nie moze byc zerowa! Koncze program!");
			System.exit(-1);
		}
		
		d = a[1]*a[1] - 4*a[0]*a[2];
		if(d>=0) {
			x1 = (-a[1]-d)/2*a[0];
			System.out.println("Pierwsze miejsce zerowe to: " + x1);
			x2 = (-a[1]+d)/2*a[0];
			System.out.println("Drugie miejsce zerowe to: " + x2);
	
			if(x1==x2) System.out.println("Delta=0. Równanie ma dwa pierwiastki rzeczywiste równe");
		}else {
			x1 = (-a[1]-d)/2*a[0];
			System.out.println("Pierwsze miejsce zerowe to: " + x1);
			x2 = (-a[1]+d)/2*a[0];
			System.out.println("Drugie miejsce zerowe to: " + x2);
			System.out.println("Delta<0. Równanie ma dwa pierwiastki zespolone sprzężone");

		}
		
	}
	
//--------------------------------------------------------------------------------------------------------	
	void pochodna () {
		double p[]= new double[2];
		
		System.out.println(wzor+" -> baza do obliczenia pochodnej");
		//String tmp[] = wzor.split("\"\\\\*?x?X?[xX]{1}\\\\^?2?\" ");
				// "\\*?x?X?[xX]{1}\\^?2?"
		String tmp[] = wzor.split(" "); // podział po spacji
		
		
		wzorPochodna="";
		int rozmiar=0;
		for (String val:tmp) {
			
			System.out.println(val+" tablica "+rozmiar+": "+tmp[rozmiar]);
			String w[]=tmp[rozmiar].split("\\*?x?X?[xX]{1}\\^?");
			//  "\\*?x?X?[xX]{1}\\^?2?"
			
				int wi=0;
				for (String value:w) {
					if(value.isEmpty()||value.equals("+")||value.equals("-")) { //dodaje wartosc 1 gdy string pusty
						if (w[wi].equals("-")) {	
							w[wi]="-1";
							 p[wi]=Double.valueOf(w[wi]);
						}else{
							w[wi]="1";
							p[wi]=Double.valueOf(w[wi]);
						}	
					}else {		
						p[wi]=Double.valueOf(w[wi]);
					}
					//System.out.println(Double.valueOf(w[wi]));
					System.out.println(value+" wyraz nr "+wi);
					p[wi]=Double.valueOf(w[wi]);
					wi++;
				}
				
				
				if (wi<=1) {
					p[1]=1;
				}
				if(p[0]==0) {
					//wzorPochodna=wzorPochodna+"+"+0;
				}else {
					System.out.println(p[0]+" i "+p[1]);
					wzorPochodna=wzorPochodna+"+"+Double.toString(p[0]*p[1])+"x^"+Double.toString(p[1]-1);
					
				}
			System.out.println("Pochodna: "+wzorPochodna);
			rozmiar++; // ustalenie rozmiaru tablicy dla przechowania a,b,c,....
		}
		
		
	}
}
