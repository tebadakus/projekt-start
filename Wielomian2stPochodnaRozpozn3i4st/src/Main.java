/*
 * DO ZROBIENIA:
 * - wyeliminowac problem przetawienia szyku wielomianu (np. zamiast 2x^2+3x-2 -> 3x-2+2x^2)
 * - obsluzyc pozostale wyjatki i ewentualne bledy
 * - "nauczyc" program osblugi wielomianow wyzszego stopnia
 * 
 * PROGRAM + ALGORYTM
 * - napisac program wyliczajacy pochodne I i II stopnia z podanych wielomianow
 * 
 * - program do RPN (Reverse Polish Notation); wersja prosta - uzytkownik podaje RPN
 * - wersja optymalna - zamieniamy notacje tradycyjna na RPN (i obliczamy)
 * 
 * Dostarczenie - link do gitlaba na adres piotr_dobosz@int.pl
 */

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		do {
		new Main().wykonajDzialanie();
		}while(!wzor.equals("q"));
		
	}

	
	static String wzor=" ";
	double a[]; // parametry wielomianu a,b,c,....
	double d=0;
	double x1=0;
	double x2=0;
	String wzorPochodna="";
	int typWielomianu=0;// zero oznacza 2-gi stopien wielomianu
	boolean breakIf=false;
	String tempDodatkowyZnak="+";	
	
//-----------------------------------------------------------------------------------------------	
	@SuppressWarnings("resource")
	void wykonajDzialanie() {	
		println("");
		println("Rozwiązuję równania 2-go stopnia.\n"
				+ "Obliczam pochodne równan 2,3,4-go stopnia\n"
				+ "q - Koniec programu\n");
		System.out.print("Podaj swoj wielomian  stopnia w formie\n"
				+ " (a3)x^4 + (a2)x^3 + (a1)x^2 + bx + c : ");
		wzor=new Scanner(System.in).nextLine();
		if (wzor.equals("q"))
			System.exit(-1);
		sortujWielomian();// sortuje wielomian do postaci x^n+x^(n-1)+....		
	}

//------------------------------------------------------------------------------------------------
	/**
	 * Sprawdza czy wielomian ma znak + lub - przed wyrazem o najwższej potędze
	 * np -x^2 lub +x^3. Jeśli znak występuje usuwa go i  zapamiętuje globalnie,
	 * jako zmienną 'tempDodatkowyZnak'
	 */
	void sprawdzDodatkowyZnak() {
		if(wzor.substring(0, 1).equals("-") ) {
			tempDodatkowyZnak="";
			tempDodatkowyZnak="-";
			wzor=wzor.substring(1, wzor.length());
		}else if(wzor.substring(0, 1).equals("+")) {
			tempDodatkowyZnak="";
			tempDodatkowyZnak="+";
			wzor=wzor.substring(1, wzor.length());
		}
	}
	
	
//------------------------------------------------------------------------------------------------
	/**
	 *  Sprawdza podany wielomian i gdy nie jest pełny dodaje brakujące elementy, żeby
	 *  po rozbiciu na wyrazy tablica miala odpowiednia ilosc elementow.
	 *  np. x^3    ->   x^3 + 0x^2 + 0x + 0
	*/		
	void  uzupelnijWielomian() {
				 		int howManyX=0;
				int isTherePlusMinus=0;
			
			sprawdzDodatkowyZnak();
				
				for (int ind=0;ind <wzor.length();ind++) {
				if(wzor.substring(ind, ind+1).equals("x")) 
					howManyX++;
				if(wzor.substring(ind, ind+1).equals("+")||wzor.substring(ind, ind+1).equals("-"))
						isTherePlusMinus++;	
				}
				if (howManyX>2+typWielomianu) 
					zlyWielomian();
					
				if (isTherePlusMinus>2+typWielomianu)
					zlyWielomian();
					
				
				if (howManyX==(2+typWielomianu) && isTherePlusMinus==(1+typWielomianu)) {
					wzor=wzor+"+0";
				}else if (howManyX==(1+typWielomianu) && isTherePlusMinus==(0+typWielomianu)) {
					wzor=wzor+"+0x"+"+0";
				}else if (howManyX==(1+typWielomianu) && isTherePlusMinus==(1+typWielomianu)){
					
					wzor=wzor+"+0x";
				}
				
				//*************************************************************
		breakIf=true;		
	}
	 
	 
//---------------------------------------------------------------------------------------------	 
	/**
	 * Wywołuje metodę początkową 'wykonajDziałanie()', jeśli podany wielomian nie spełnił
	 * warunków początkowych
	 */
	void zlyWielomian() {
		 println("Podales zly rodzaj wielomianu! Wprowadz jeszcze raz");
		 wykonajDzialanie();
	 }
	 
	 
	 
//---------------------------------------------------------------------------------------------
	/**
	 *  Oczyszcza wpisany wielomian z niepożądanych znaków omyłkowo wpisanych.
	 *  Sortuje wielomian jeśli wyrazy wielomianu nie zostały podane w prawidłowej kolejności.
	 * 				Przykład:  x - x^2 + 2  --->  -x^2 + x + 2 .
	 *  Dopisuje brakujące wyrazy w wielomianach powyżej drugiego stopnia
	 *  			Przykład: x^4 + x^2 ---> x^4 + 0x^3 + x^2 
	 */
	
	void sortujWielomian() {
		wzor=wzor.replaceAll("[\\\\#!@%`\\*()_=~{}\\[\\]\\?/<>,.|]?", "");
		wzor=wzor.replaceAll("[a-zA-Z&&[^xX]]?", "");
		wzor=wzor.replace(" ","");	
				
		if (!wzor.contains("^2")&&!wzor.contains("^3")&&!wzor.contains("^4") || wzor.indexOf('^')==-1) {
				System.out.println(wzor.split("\\^[^2]{1}")[0]);
				zlyWielomian();
		}
		
	//*************sprawdzenie i dopisanie wyrazow x^4, x^3, x^2************************
		if(wzor.contains("x^4")&&wzor.contains("x^3")&&wzor.contains("x^2")&&!breakIf) {
			typWielomianu=2;
			uzupelnijWielomian();  
		}else if(wzor.contains("x^4")&&wzor.contains("x^3")&&!breakIf) {
			wzor=wzor+"+0x^2"; 
			typWielomianu=2;
			uzupelnijWielomian();
		}else if(wzor.contains("x^4")&&wzor.contains("x^2")&&!breakIf) {
			wzor=wzor+"+0x^3";
			typWielomianu=2;
			uzupelnijWielomian();
		}else if(wzor.contains("x^4")&&!breakIf) {
			wzor=wzor+"+0x^3+0x^2";
			typWielomianu=2;
			uzupelnijWielomian();
		}else if (wzor.contains("x^3")&&wzor.contains("x^2")&&!breakIf) {
			typWielomianu=1;
			uzupelnijWielomian();
		}else if (wzor.contains("x^3")&&!breakIf) { 
			wzor=wzor+"+0x^2";
			typWielomianu=1;
			uzupelnijWielomian();
		}else {// Jezeli podano wielomian drugiego stopnia 
			uzupelnijWielomian();
		}  
		
		przygotujWzorDoPodzialu();
		String tmp[] = wzor.split(" "); // podział po spacji
		tmp[0]=tempDodatkowyZnak+tmp[0]; // dopisanie przed pierwszym wyrazem zapamiętanego globalnie znaku + lub -
		

	//************sortowanie wyrazow wielomianu - ustawienie w kolejnosci**************
		String temporary="";
		String wPotega="";
		int potega=2+typWielomianu;
		int zakres=potega;

		for(int j=0;j<=zakres;j++) {
			if((tmp[j].substring(tmp[j].length()-1, tmp[j].length())).equals(Integer.toString(potega))) {
				if (j!=(2+typWielomianu-potega)) {
					wPotega="x^"+Integer.toString(potega);
					if(tmp[j].contains(wPotega)) {
						temporary=tmp[2+typWielomianu-potega];
						tmp[2+typWielomianu-potega]=tmp[j];
						tmp[j]=temporary;
						potega--;
						j=-1;
					}
				// nie rob nic bo to ostatni element wielomianu bez x
				}
			}else if((tmp[j].substring(tmp[j].length()-1, tmp[j].length())).equals("x")) {
				if(j!=1+typWielomianu) {
					temporary=tmp[1+typWielomianu];
					tmp[1+typWielomianu]=tmp[j];
					tmp[j]=temporary;
					j=-1;
				}
			}			
		}
		wzor="";
		for(String val:tmp) {
			wzor+=val;
		}
		System.out.println("Twój wzor do obliczeń: "+wzor);

	//*******************Określenie jaki typ wielomianu został podany*****************************
		if(typWielomianu==0) {
			println("Podałeś wielomian 2-go stopnia, mogę obliczyć miejsca zerowe i pochodną");	
			znajdzZmienne(); // wyszykuje parametry wielomianu a,b,c
			obliczPochodne();
		} else if(typWielomianu==1) {
			println("Podałeś wielomian 3-go stopnia mogę obliczyć pochodną 1-go i 2-go stopnia");
			obliczPochodne();
		} else if(typWielomianu==2) {
			println("Podałeś wielomian 4-go stopnia mogę obliczyć pochodną 1-go i 2-go stopnia");
			obliczPochodne();
		}
	}
//-----------------------------------------------------------------------------------------
	/**
	 * Wywołuje metodę 'pochodna()' w celu obliczenia pierwszej i drugiej pochodnej. 
	 * Nowy Scanner jest wywołany w celu oczekiwania na przyciśnięcie klawisza przed 
	 * ponownym uruchomieniem programu. 
	 */
	@SuppressWarnings("resource")
	 void obliczPochodne(){
		 	pochodna();
			println(" - Pierwsza pochodna");
			pochodna();
			println(" - Druga pochodna");
			println("Wcisnij Enter aby kontynuować");
			new Scanner(System.in).nextLine();
	 }
//-----------------------------------------------------------------------------------------	
	/**
	 * Drukuje zadany opis z podkresleniem wzdłuż calej lini.
	 * @param opis
	 */
	void println(String opis) {
		System.out.println(opis);
		System.out.println("-------------------------------------------------------------------------");
	}	
	
//-----------------------------------------------------------------------------------------
	/**
	 * Wyodrębnia współczynniki a, b, c wielomianu drugiego stopnia.
	 * Następnie z wyodrębnionych elementów oblicza 
	 * deltę = a^2 - 4*a*c
	 * oraz miejsca zerowe wielomianu
	 * x1 = (-b -delta)/2*a , x2 = (-b +delta)/2*a 
	 */
	void znajdzZmienne( ) {
		przygotujWzorDoPodzialu();
		String tmp[] = wzor.split(" ");  // podział po spacji
		tmp[0]=tempDodatkowyZnak+tmp[0]; //dopisanie do pierwszego podanego czynnika wielomianu znaku
		
				
		// Wyodrębnienie współczyników wielomianu z poszczególnych wyrazów wielomianu.
		for (int rozmiar=0; rozmiar<=tmp.length-1;rozmiar++) {
			String temp[]=tmp[rozmiar].split("\\*?x?X?[xX]{1}\\^?2?");
			tmp[rozmiar]=temp[0];
		}
		
		a=new double[tmp.length];
		int i=0;
		for (String val:tmp) {
			val.replace(" ","");
			if(val.isEmpty()||val.equals("+")||val.equals("-")) { //wstawia 1 gdy string pusty
				if (tmp[i].equals("-")) {	
					tmp[i]="-1";
					 a[i]=Double.valueOf(tmp[i]);
				}else{
					tmp[i]="1";
					a[i]=Double.valueOf(tmp[i]);
				}	
			}else {		
				a[i]=Double.valueOf(tmp[i]);
			}
			System.out.println("Współczynnik wielomianu "+(char)(i+97)+" = "+ Double.valueOf(tmp[i]));
		i++;
		}

		
		if  (a[0]==0) { 
			println("Wartosc a  nie moze byc zerowa! Koncze program!");
			zlyWielomian();
		}
		
		System.out.println("Delta = "+(d = a[1]*a[1] - 4*a[0]*a[2]));
			x1 = (-a[1]-d)/2*a[0];
			System.out.println("Pierwsze miejsce zerowe: x1 = " + x1);
			x2 = (-a[1]+d)/2*a[0];
			System.out.println("Drugie miejsce zerowe: x2 = " + x2);
			if(x1==x2) println("Delta = 0. Równanie ma dwa pierwiastki równe");	
			println("");
			
	}
	
	
//------------------------------------------------------------------------------------------
	/**
	 * Usuwa zbędne spacje, następnie wstawia spacje przed znakiem + i - 
	 * aby przygotować wzór do podziału na wyrazy w miejscu spacji przed znakiem.
	 */
	void przygotujWzorDoPodzialu() {
		wzor=wzor.replace(" ","");
		sprawdzDodatkowyZnak();
		wzor=wzor.replace("+", " +"); 		 
		wzor=wzor.replace("-", " -"); 		
	}
	
//------------------------------------------------------------------------------------------	
	/**
	 * Oblicza pierwszą pochodną z zadanego wielomianu.
	 * Ponowne wywołanie metody oblicza drugą pochodną.
	 * Przykład pochodna z c'=0, 2x'=2, 2x^2'=4x 
	 */
	void pochodna () {
		double p[]= new double[2];
		// 3+2x+2x^2+x^3+x^4	-ok
		// 4x+4x^2+4x^4+4+4x^3	-ok
		// 3+3x+3x^3+3x^2	-ok
		// 2+x^2+x+x^3		-ok
		// x+2+x^2+x^3		-ok
		// 5+x+x^2			-ok
		// x+5+x^2			-ok
		// x^3+x^2+x+x^4+2	-ok
		// x^4+x^3+x^2+x	-ok
		
		System.out.println("Obliczam pochodną z podanego wzoru");
		przygotujWzorDoPodzialu();
		String tmp[] = wzor.split(" "); // podział po spacji
		tmp[0]=tempDodatkowyZnak+tmp[0]; 
		// dodaje wcześniej zapamiętany globalnie znak przed pierwszym wyrazem wielomianu.
		wzorPochodna="";
		int rozmiar=0;
	
		for (String val:tmp) {
			String w[]=tmp[rozmiar].split("\\*?x?X?[xX]{1}\\^?");
				int wi=0;
				for (String value:w) {
					if(value.isEmpty()||value.equals("+")||value.equals("-")) { 
						//dodaje wartosc 1 gdy string jest pusty lub zawiera tylko znak.
						if (w[wi].equals("-")) {	
							w[wi]="-1";
							 p[wi]=Double.valueOf(w[wi]);
						}else{
							w[wi]="1";
							p[wi]=Double.valueOf(w[wi]);
						}	
					}else {		
						p[wi]=Double.valueOf(w[wi]);
					}
					wi++;
				}
				if (p[0]==0) {
				}else if (tmp[rozmiar].contains("x")&&p[1]==1) {
					wzorPochodna=wzorPochodna+znakPochodna(tmp[rozmiar].toString())+Double.toString(p[0]*p[1]);
				}else if(tmp[rozmiar].contains("x")&&p[1]==0){
					wzorPochodna=wzorPochodna+znakPochodna(tmp[rozmiar].toString())+p[0];
				}else if (tmp[rozmiar].contains("x")&&p[1]!=0) { //(tmp[rozmiar].contains("x")&&p[1]!=0)
					wzorPochodna=wzorPochodna+znakPochodna(tmp[rozmiar].toString())+Double.toString(p[0]*p[1])+"x^"+Double.toString(p[1]-1);
				}
			p[1]=0;
			rozmiar++; 
		}
		System.out.print(wzorPochodna);
		wzor=wzorPochodna;
	}
	
//-----------------------------------------------------------------------------------------------
	/**
	 * Metoda sprawdza czy znak wyrazu wielomianu jest ujemny.
	 * Jeśli znak jest ujemny to przy wyświetlaniu nie wstawia, 
	 * żadnego znaku do wzoru pochodnej przed wyrazem. 
	 * @param wyraz
	 * @return
	 */
	String znakPochodna(String wyraz) {
		 if(wyraz.substring(0, 1).equals("-")) 
			 return " ";
		 return " +";
	 }
//-----------------------------------------------------------------------------------------------
}
