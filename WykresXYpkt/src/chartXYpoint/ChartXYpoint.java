package chartXYpoint;
import java.util.Scanner;

public class ChartXYpoint {
	
	public static void main(String[] args) {
		try {
		String choos ="y";
		while (!choos.equals("q")) {
			Scanner input=new Scanner(System.in);
			System.out.println("Give coordynates of points as int from 0 to 10: ");
			System.out.println("X coordynates: ");
			int px=input.nextInt();
			System.out.println("Y coordynates: ");
			int py=input.nextInt();		
		
		// draws chart axis
	for (int y=0; y<11; y+=1) {
		for (int x=0; x<11; x+=1) {
			if (x==px && y==py) {
				if (x==0) {System.out.print("  ,"); }
				else {System.out.print(" ,"); }
			}
			else if (y==0 && x==0) {	System.out.print("  "+x+""); }
			else if (y==0 && x<10) {System.out.print("_"+x);}
			else if (y==0 && x==10) {System.out.println("_"+x); }
			else if (x==0 && y!=10) {System.out.print(" "+y+"|");}
			else if (y==10 && x==0) {System.out.print(y+"|");}	
			else if (x==10) {System.out.println(" ."); }
			else {System.out.print(" ."); }		
		}
	}			
			System.out.println("Quit-  'q', Work again- 'y'");		
				 choos = input.next();
				 if (choos.equals("q")) input.close(); 			
		}
	}
		catch (Exception e) {
			System.err.println("Error- Try again "+e);
		}
	}
}
