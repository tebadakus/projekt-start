package zadanieWycinanieZPlyty;

public class ElementWycinany {
	private int szerokosc,
					wysokosc;

	public int getSzerokosc() {
		return szerokosc;
	}

	public void setSzerokosc(int szerokosc) {
		this.szerokosc = szerokosc;
	}

	public int getWysokosc() {
		return wysokosc;
	}

	public void setWysokosc(int wysokosc) {
		this.wysokosc = wysokosc;
	}

	
}
