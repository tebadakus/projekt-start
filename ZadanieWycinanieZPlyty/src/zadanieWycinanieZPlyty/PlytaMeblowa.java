package zadanieWycinanieZPlyty;

public class PlytaMeblowa {
	private String  typPlyty="mdf",
					kolorPlyty="machon";
				int	gruboscPlyty=25,
					szerokoscPlyty=3000,
					wysokoscPlyty=2000;

				/*
				 * sze % szeE  <  sze % wysE  -T
				 * wys % wysE  <  wys % szeE  -T
				 *
				 *  ss=0
					ww=600
					rest1 = ww*sze +ss*(wys-ww)

					sw=200
					ws=200
					rest2 = sw*wys +ws*(sze-sw)

  					  T	       F
					ss<sw && ww<ws

  						F	   T
					sw<ss && ws<ww
			
				 */
				
	public String IleElementowNaPlycie(int szerokosc, int wysokosc) {
		int ss = this.szerokoscPlyty%szerokosc,
			sw = this.szerokoscPlyty%wysokosc,
			ww = this.wysokoscPlyty%wysokosc,
			ws = this.wysokoscPlyty%szerokosc,
			rest1 = ww*szerokosc +ss*(wysokosc-ww),// ss & ww - tyle materialu zostaje-odpad
			rest2 = sw*wysokosc +ws*(szerokosc-sw);// sw & ws - tyle materialu zostaje-odpad
		
		if (rest1 < rest2)  // sprawdzenie dla którego przypadku zostanie mniej materiału i wybranie tej wersji.
			return "Można wyciąć elementów: "+((this.szerokoscPlyty/szerokosc) * (this.wysokoscPlyty/wysokosc))+" pozostało "+rest1/1e6+" m2";
		
			return "Można wyciąć elementów: "+((this.szerokoscPlyty/wysokosc) *( this.wysokoscPlyty/szerokosc))+" pozostało "+rest2/1e6+" m2";
	}
	
	
	

	public int getSzerokoscPlyty() {
		return szerokoscPlyty;
	}



	public void setSzerokoscPlyty(int szerokoscPlyty) {
		 this.szerokoscPlyty = szerokoscPlyty;
	}



	public int getWysokoscPlyty() {
		return wysokoscPlyty;
	}



	public void setWysokoscPlyty(int wysokoscPlyty) {
		this.wysokoscPlyty = wysokoscPlyty;
	}


	public String getTypPlyty() {
		return typPlyty;
	}

	public void setTypPlyty(String typPlyty) {
		this.typPlyty = typPlyty;
	}

	public String getKolorPlyty() {
		return kolorPlyty;
	}

	public void setKolorPlyty(String kolorPlyty) {
		this.kolorPlyty = kolorPlyty;
	}

	public int getGruboscPlyty() {
		return gruboscPlyty;
	}

	public void setGruboscPlyty(int gruboscPlyty) {
		this.gruboscPlyty = gruboscPlyty;
	}

	
}
