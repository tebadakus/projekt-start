package zadanieWycinanieZPlyty;

/*
 * Zadanie 1
 * Napisać program pozwalający na obliczanie ilości elementów, 
 * jakie można by było wyciąć z płyty meblowej. Należy pamiętać, 
 * że każda płyta ma swój wymiar (szerokość oraz wysokość). 
 * Elementy wycinane w uproszczeniu przyjmiemy jako prostokąty.
 * 
 * Wykonanie programu:
 * v- Utworzyć klasę pozwalającą zapisywać informacje o płytach meblowych 
 *   (podać minimum 5 parametrów; najistotniejsze to szerokość oraz wysokość)
 * v- klasę pozwalającą na zapisanie informacji o wycinanych elementach;
 *   na początek przyjmiemy tworzenie tylko jednego elementu;
 * v- w klasie płyty należy utworzyć metodę pozwalającą obliczyć ile elementów
 *   będziemy w stanie utworzyć dla bieżącej wielkości elementu;
 * - utworzyć dodatkową metodę podającą ile elementów o podanym wymiarze  
 *   można utworzyć oraz powierzchni pozostałej po wycince
 */


public class ZadanieWycinanieZPlyty {
public static void main(String[] args) {
	
	PlytaMeblowa pm=new PlytaMeblowa();
	ElementWycinany ew=new ElementWycinany();
	
	//Element do wyciecia
	ew.setSzerokosc(600);
	ew.setWysokosc(400);
	
	System.out.println(pm.getTypPlyty());
	System.out.println(ew.getSzerokosc());
	System.out.println(ew.getWysokosc());
	
	System.out.println(pm.IleElementowNaPlycie(ew.getSzerokosc(), ew.getWysokosc()));
}
}
